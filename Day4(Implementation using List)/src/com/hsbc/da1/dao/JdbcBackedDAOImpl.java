package com.hsbc.da1.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.hsbc.da1.exception.CustomerNotFoundException;
import com.hsbc.da1.model.SavingsAccount;

public class JdbcBackedDAOImpl implements SavingsAccountDAO {

	private static String connectString = "jdbc:derby://localhost:1527/sidhantdatabase";
	private static String username = "admin";
	private static String password = "password";

	private static final String INSERT_QUERY = "insert into savings_account (account_number, cust_name, account_balance)"
			+ " values ";

	private static final String SELECT_QUERY = "select * from savings_account";

	private static final String DELETE_BY_ID_QUERY = "delete  from savings_account where account_number=";
	
	private static final String SELECT_BY_ID_QUERY = "select *  from savings_account where account_number=";

	private static Statement getStatement() {
		try {
			Connection connection = DriverManager.getConnection(connectString, username, password);
			System.out.println("Successfully connected to the database ");
			return connection.createStatement();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount) {
		String query = INSERT_QUERY + " (" + savingsAccount.getAccountNumber() + ", " +"'"+ savingsAccount.getAccountName()+"'"
				+ "," + savingsAccount.getAccountBalance() + ")";
		System.out.println("query submitt ed to the db :  "+ query);
		int rowsUpdated = 0;
		try {
			rowsUpdated = getStatement().executeUpdate(query);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (rowsUpdated == 1) {
			return savingsAccount;
		}
		return null;
	}

	@Override
	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount) {
		
		return null;
	}

	@Override
	public void deleteSavingsAccount(long accountNumber) {
		// TODO Auto-generated method stub

	}

	@Override
	public Set<SavingsAccount> fetchSavingsAccounts() {
		String query = SELECT_QUERY;
		Set<SavingsAccount> savingsAccountSet = new HashSet<>();
		try {
			ResultSet rs = getStatement().executeQuery(query);
			if ( rs.next()) {
				SavingsAccount savingsAccount = 
						new SavingsAccount(rs.getString("cust_name"), rs.getDouble("account_balance"));
				savingsAccountSet.add(savingsAccount);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return savingsAccountSet;
	}

	@Override
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws CustomerNotFoundException {
		String query = SELECT_BY_ID_QUERY+ " "+ accountNumber;
		try {
			ResultSet rs = getStatement().executeQuery(query);
			if ( rs.next()) {
				SavingsAccount savingsAccount = 
						new SavingsAccount(rs.getString("cust_name"), rs.getDouble("account_balance"));
				return savingsAccount;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new CustomerNotFoundException(" Customer with "+ accountNumber+ " does not exists");
		}
		return null;
	}

}
