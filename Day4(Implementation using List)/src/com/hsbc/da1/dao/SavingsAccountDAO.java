package com.hsbc.da1.dao;

import java.util.List;
import java.util.Set;
import java.util.Collection;

import com.hsbc.da1.exception.CustomerNotFoundException;
import com.hsbc.da1.model.SavingsAccount;

public interface SavingsAccountDAO {
	
	
	SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount);
	
	SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount);
	
	void deleteSavingsAccount(long accountNumber);
	
	Set<SavingsAccount> fetchSavingsAccounts();
	
	
	SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws CustomerNotFoundException;

}
