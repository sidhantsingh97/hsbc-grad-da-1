package com.hsbc.da1.dao;

import java.util.Arrays;
import java.util.List;

import com.hsbc.da1.exception.CustomerNotFoundException;
import com.hsbc.da1.model.SavingsAccount;

public class ArrayBackedSavingsAccountDAOImpl implements SavingsAccountDAO {

	private static SavingsAccount[] savingsAccounts = new SavingsAccount[100];
	private static int counter;
	
	public SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount) {
		savingsAccounts[counter++] = savingsAccount;
		return savingsAccount;
	}
	
	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount) {
		for (int index = 0; index < savingsAccounts.length; index ++) {
			if ( savingsAccounts[index].getAccountNumber() == accountNumber) {
				savingsAccounts[index] = savingsAccount;
				break;
			}
		}
		return savingsAccount;
	}
	
	public void deleteSavingsAccount(long accountNumber) {
		
		for (int index = 0; index < savingsAccounts.length ; index ++) {
			if ( savingsAccounts[index ] != null && savingsAccounts[index].getAccountNumber() == accountNumber) {
				savingsAccounts[index] = null;
				break;
			}
		}
	}
	
	public List<SavingsAccount> fetchSavingsAccounts() {
		return Arrays.asList(savingsAccounts);
	}
	
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws CustomerNotFoundException{
		for (int index = 0; index < savingsAccounts.length; index ++) {
			if ( savingsAccounts[index ] != null &&  savingsAccounts[index].getAccountNumber() == accountNumber) {
				return savingsAccounts[index];
			}
		}
		throw new CustomerNotFoundException("Invalid customer id");
	}

}
