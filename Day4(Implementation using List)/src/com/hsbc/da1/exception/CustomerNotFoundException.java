package com.hsbc.da1.exception;

public class CustomerNotFoundException extends Exception {
	
	public CustomerNotFoundException(String message) {
		super(message);
	}

	public String getMessage() {
		return super.getMessage();
	}
}
