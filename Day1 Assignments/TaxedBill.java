public class TaxedBill
{
    public static void main(String args[])
    {
        String billAmount = args[0];
        float floatBillValue = Float.parseFloat(billAmount);
        float totalAmount = floatBillValue+((float)0.15*floatBillValue);
        System.out.println("Total amount after applying tax is : "+totalAmount);
    }
}