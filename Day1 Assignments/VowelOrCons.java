public class VowelOrCons
{
    public static void main(String args[])
    {
        char c = args[0].charAt(0);
        int val =(int)c;
        if((val>=65 && val<=90) || (val>=97 && val<=122))
        { 
        if(c == 'a' || c == 'e' ||c == 'i' ||c == 'o' ||c == 'u' ||c == 'A' ||c == 'E' ||c == 'I' ||c == 'O' ||c == 'U') 
        System.out.println("Vowel");
        else
        System.out.println("Consonant");
        }
        else
        System.out.println("Enter a valid character i.e. any alphabet");
    }
}