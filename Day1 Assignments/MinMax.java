public class MinMax
{
    public static void main(String args[])
    {
        int inputs[]= new int[args.length];
        int counter;
        if(args.length==5)
        {
        for(counter=0;counter<args.length;counter++)
        {
            String num = args[counter];
            inputs[counter] = Integer.parseInt(num);
        }
        int minValue = inputs[0];
        for(counter=1;counter<args.length;counter++)
        {
            if(minValue>inputs[counter])
            minValue=inputs[counter];
        }
        System.out.println("The minimum value entered is : "+minValue);
        int maxValue = inputs[0];
        for(counter=1;counter<args.length;counter++)
        {
            if(maxValue<inputs[counter])
            maxValue=inputs[counter];
        }
        System.out.println("The maximum value entered is : "+maxValue);
        }
        else
        System.out.println("Please enter only 5 values.");
    }
}