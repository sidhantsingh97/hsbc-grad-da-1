public class StateCodeTotalBill
{
    public static void main(String args[])
    {
        String billAmount = args[0];
        String stateCode = args[1];
        float floatBillValue = Float.parseFloat(billAmount);
        float totalAmount;
        if(stateCode.equals("KA"))
        {
            totalAmount = floatBillValue+((float)0.15*floatBillValue);
        }
        else if(stateCode.equals("TN"))
        {
            totalAmount = floatBillValue+((float)0.18*floatBillValue);
        }
        else if(stateCode.equals("MH"))
        {
            totalAmount = floatBillValue+((float)0.20*floatBillValue);
        }
        else
        {
             totalAmount = floatBillValue+((float)0.12*floatBillValue);
        }
        System.out.println("Total bill after applying tax for state: "+stateCode+" is : "+totalAmount);
    }
}