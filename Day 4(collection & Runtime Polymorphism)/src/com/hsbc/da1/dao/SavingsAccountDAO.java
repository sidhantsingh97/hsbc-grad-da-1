package com.hsbc.da1.dao;

import java.util.Collection;
import java.util.List;

import com.hsbc.da1.exception.CustomerNotFoundException;
import com.hsbc.da1.model.SavingsAccount;

public interface SavingsAccountDAO {
	
	
	SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount);
	
	SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount);
	
	void deleteSavingsAccount(long accountNumber);
	
	Collection<SavingsAccount> fetchSavingsAccounts();
	
	
	SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws CustomerNotFoundException;
	

}
