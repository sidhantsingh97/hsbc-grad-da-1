package com.hsbc.da1.service;

import com.hsbc.da1.exception.CustomerNotFoundException;
import com.hsbc.da1.model.*;
import com.hsbc.da1.exception.*;
public interface SavingsAccountService {

	public SavingsAccount createSavingAccount(String customerName, double amount);
	
	public void deleteSavingsAccount(long accountNumber)throws CustomerNotFoundException;
	
	public SavingsAccount fetchSavingsAccountById(long accountNumber)throws CustomerNotFoundException;
	
	public SavingsAccount[] fetchSavingsAccount();
	
	public double deposit(long accountNumber, double amount)throws CustomerNotFoundException;
	
	public double withdraw(long accountNumber, double amount)throws CustomerNotFoundException;
	
	public void transfer(long fromacc, long toacc,double amount)throws CustomerNotFoundException;
	
	public double checkBalance(long accountNumber)throws CustomerNotFoundException;
}
