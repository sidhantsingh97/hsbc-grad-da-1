package com.hsbc.da1.service;

import com.hsbc.da1.model.*;
import com.hsbc.da1.exception.*;
import com.hsbc.da1.dao.*;
import com.hsbc.da1.exception.CustomerNotFoundException;
import com.hsbc.da1.util.*;
public class SavingsAccountServiceImpl implements SavingsAccountService{
	
	SavingsAccountDAO dao = SavingsAccountDAOFactory.getSavingsAccountDAO(1);

	@Override
	public SavingsAccount createSavingAccount(String customerName, double accountBalance) {
		SavingsAccount savingsAccount = new SavingsAccount(customerName, accountBalance);
		SavingsAccount savingsAccountCreated = this.dao.saveSavingsAccount(savingsAccount);
		return savingsAccountCreated;
	}

	@Override
	public void deleteSavingsAccount(long accountNumber)throws CustomerNotFoundException {
		// TODO Auto-generated method stub
		SavingsAccount sa = dao.fetchSavingsAccountById(accountNumber);
		if(sa!=null)
		this.dao.deleteSavingsAccount(accountNumber);
		else
			System.out.println("No such account exists..");
		
	}

	@Override
	public SavingsAccount fetchSavingsAccountById(long accountNumber) throws CustomerNotFoundException{
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountById(accountNumber);
		return savingsAccount;
	}

	@Override
	public SavingsAccount[] fetchSavingsAccount() {
		// TODO Auto-generated method stub
		SavingsAccount[] sa = dao.fetchSavingsAccount();
		return sa;
	}

	@Override
	public double deposit(long accountNumber, double amount)throws CustomerNotFoundException {
		// TODO Auto-generated method stub
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountById(accountNumber);
		if (savingsAccount != null) {
			double currentAccountBalance = savingsAccount.getAccountBalance();
			savingsAccount.setAccountBalance(currentAccountBalance + amount);
			this.dao.updateSavingsAccount(accountNumber, savingsAccount);
			return savingsAccount.getAccountBalance();
		}
		return 0;
	}

	@Override
	public double withdraw(long accountNumber, double amount)throws CustomerNotFoundException {
		// TODO Auto-generated method stub
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountById(accountNumber);
		if (savingsAccount != null) {
			double currentAccountBalance = savingsAccount.getAccountBalance();
			if ( currentAccountBalance >= amount) {
				currentAccountBalance = currentAccountBalance - amount;
				savingsAccount.setAccountBalance(currentAccountBalance);
				this.dao.updateSavingsAccount(accountNumber, savingsAccount);
				return amount;
			}
		}
		return 0;
	}

	@Override
	public void transfer(long fromacc, long toacc, double amount)throws CustomerNotFoundException {
		// TODO Auto-generated method stub
		SavingsAccount fromAccount = this.dao.fetchSavingsAccountById(fromacc);
		SavingsAccount toAccount = this.dao.fetchSavingsAccountById(toacc);
		double updatedBalance = this.withdraw(fromAccount.getAccountNumber(), amount);
		if ( updatedBalance != 0) {
			this.deposit(toAccount.getAccountNumber(), amount);
		}
	}

	@Override
	public double checkBalance(long accountNumber) throws CustomerNotFoundException{
		// TODO Auto-generated method stub
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountById(accountNumber);
		if (savingsAccount != null) {
			return savingsAccount.getAccountBalance();
		}
		return 0;
	}

	
	
}
