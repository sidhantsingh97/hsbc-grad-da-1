<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import = "java.util.*,com.hsbc.da1.model.SavingsAccount" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Savings Account Fetched By Account Id</title>
<style>
  table {
    border: 1px solid red;
  }
  td, tr {
    border: 1px solid red;
  }
</style>
</head>
<body>
		<%
         SavingsAccount sa = (SavingsAccount)request.getAttribute("savingsaccount");
        %>
        
        
        <table>
    <thead>
      <tr>
       <th>ACCOUNT NUMBER</th>
       <th>CUSTOMER NAME</th>
       <th>E-MAIL ADDRESS</th>
       <th>ACCOUNT BALANCE </th>
      </tr> 
    </thead>
    <tbody>
          <tr>
             <td>
              <%= sa.getAccountNumber() %>
             </td>
          
             <td>
              <%= sa.getCustomerName() %>
             </td>
             <td>
              <%= sa.getEmailAddress() %>
             </td>
             <td>
              <%= sa.getAccountBalance() %>
             </td>
          </tr>
    </tbody>
  </table>

</body>
</html>