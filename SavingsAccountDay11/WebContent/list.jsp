<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.util.*,com.hsbc.da1.model.SavingsAccount,com.hsbc.da1.dao.*,com.hsbc.da1.service.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Listing all the Savings Accounts</title>
<style>
  table {
    border: 1px solid red;
  }
  td, tr {
    border: 1px solid red;
  }
</style>
</head>
<body>
	
	<%
         List<SavingsAccount> sa = (List)request.getAttribute("savingsaccount");
        %>

  <table>
    <thead>
      <tr>
       <th>ACCOUNT NUMBER</th>
       <th>CUSTOMER NAME</th>
       <th>E-MAIL ADDRESS</th>
       <th>ACCOUNT BALANCE </th>
      </tr> 
    </thead>
    <tbody>
      <%
         for ( SavingsAccount index: sa) {
      %>
          <tr>
             <td>
              <%= index.getAccountNumber() %>
             </td>
          
             <td>
              <%= index.getCustomerName() %>
             </td>
             <td>
              <%= index.getEmailAddress() %>
             </td>
             <td>
              <%= index.getAccountBalance() %>
             </td>
          </tr>
      <% 
         }
       %>
    </tbody>
  </table>
	
</body>
</html>