package com.hsbc.da1.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.hsbc.da1.model.SavingsAccount;
import com.hsbc.da1.exception.CustomerNotFoundException;

public class JdbcBackedDAOImpl implements SavingsAccountDAO {

	private static String connectString = "jdbc:derby://localhost:1527/sidhantdatabase";
	private static String username = "admin";
	private static String password = "password";

	private static final String INSERT_QUERY = "insert into savings_account (account_number, cust_name, account_balance)"
			+ " values ";

	private static final String INSERT_QUERY_PREP_STMT = "insert into savings_account (cust_name, account_balance, email_address)"
			+ " values ( ?, ?, ?)";

	private static final String SELECT_QUERY = "select * from savings_account";

	private static final String DELETE_BY_ID_QUERY = "delete  from savings_account where account_number=?";

	private static final String SELECT_BY_ID_QUERY = "select *  from savings_account where account_number=?";

	private static final String SELECT_BY_EMAIL_QUERY = "select *  from savings_account where email_address= ?";
	
	private static final String UPDATE_BY_ID_QUERY = "update savings_account set CUST_NAME = ? ,account_balance= ?, email_address = ? where account_number = ?";

	/*private static Statement getStatement() {
		try {
			Connection connection = JdbcBackedDAOImpl.getDBConnection();
			return connection.prepareStatement(sql)

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}*/

	private static Connection getDBConnection() {
		try {
			Class.forName("org.apache.derby.jdbc.ClientDriver");
			Connection connection = DriverManager.getConnection(connectString, username, password);
			return connection;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		catch(ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount) {
		int numberOfRecordsUpdated = 0;
		try (PreparedStatement pStmt = getDBConnection().prepareStatement(INSERT_QUERY_PREP_STMT);) {
			pStmt.setString(1, savingsAccount.getCustomerName());
			pStmt.setDouble(2, savingsAccount.getAccountBalance());
			pStmt.setString(3, savingsAccount.getEmailAddress());

			numberOfRecordsUpdated = pStmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (numberOfRecordsUpdated == 1) {
			// fetch the newly saved savings account from the database and return the
			// result.
			try {
				SavingsAccount fetchedSavingsAccount = fetchSavingsAccountByEmailAddress(
						savingsAccount.getEmailAddress());
				if (fetchedSavingsAccount != null) {
					return fetchedSavingsAccount;
				}

			} catch (CustomerNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount)throws CustomerNotFoundException {
		try{
			Connection con = getDBConnection();
			PreparedStatement psmt = con.prepareStatement(UPDATE_BY_ID_QUERY);
			psmt.setString(1, savingsAccount.getCustomerName());
			psmt.setDouble(2, savingsAccount.getAccountBalance());
			psmt.setString(3, savingsAccount.getEmailAddress());
			psmt.setLong(4, accountNumber);
			int noOfRecordsUpdated = psmt.executeUpdate();
			if(noOfRecordsUpdated==1)
			{
				SavingsAccount saUpdated = fetchSavingsAccountByAccountId(accountNumber);
				return saUpdated;
			}
			
		}catch(SQLException e)
		{
			e.printStackTrace();
			throw new CustomerNotFoundException("The Customer with the account number : "+accountNumber+" doesnot exist..");
		}
		return null;
	}

	@Override
	public void deleteSavingsAccount(long accountNumber) {
		int numberOfRecordsDeleted = 0;
		try (PreparedStatement pStmt = getDBConnection().prepareStatement(DELETE_BY_ID_QUERY);) {
			pStmt.setLong(1,accountNumber);
			numberOfRecordsDeleted = pStmt.executeUpdate();
			if(numberOfRecordsDeleted==1)
			{
				System.out.println("The account with account number : "+accountNumber+" was succssfully deleted");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public List<SavingsAccount> fetchSavingsAccounts() {
		List<SavingsAccount> savingsAccountList = new ArrayList<>();
		try {
			Connection con = getDBConnection();
			PreparedStatement psmt = con.prepareStatement(SELECT_QUERY);
			ResultSet rs = psmt.executeQuery();
			while(rs.next()) {
				SavingsAccount savingsAccount = new SavingsAccount(rs.getInt("account_number"),
						rs.getString("cust_name"), rs.getDouble("account_balance"), rs.getString("email_address"));
				savingsAccountList.add(savingsAccount);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return savingsAccountList;
	}

	@Override
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws CustomerNotFoundException {
		try {
			Connection con = getDBConnection();
			PreparedStatement psmt = con.prepareStatement(SELECT_BY_ID_QUERY);
			psmt.setLong(1, accountNumber);
			ResultSet rs = psmt.executeQuery();
			if (rs.next()) {
				SavingsAccount savingsAccount = new SavingsAccount(rs.getInt("account_number"),
						rs.getString("cust_name"), rs.getDouble("account_balance"), rs.getString("email_address"));
				return savingsAccount;
			}
				throw new CustomerNotFoundException(" Customer with " + accountNumber + " does not exists");
		} catch (SQLException e) {
			e.printStackTrace();
			
		}
		return null;
	}

	@Override
	public SavingsAccount fetchSavingsAccountByEmailAddress(String emailAddress) throws CustomerNotFoundException {
		
		try {
			Connection connection = getDBConnection();
			PreparedStatement ps = connection.prepareStatement(SELECT_BY_EMAIL_QUERY);
			ps.setString(1, emailAddress);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				//System.out.println("Account number: "+rs.getInt("account_number"));
				
				SavingsAccount savingsAccount = new SavingsAccount(rs.getInt("account_number"),
						rs.getString("cust_name"), rs.getDouble("account_balance"), rs.getString("email_address"));
				System.out.println("Before returning the savings account back to the customer ");
				//System.out.println(savingsAccount);
				return savingsAccount;
			}
			throw new CustomerNotFoundException(" Customer with " + emailAddress + " does not exists");
		} catch (SQLException e) {
			e.printStackTrace();
			
		}
		return null;
	}

}
