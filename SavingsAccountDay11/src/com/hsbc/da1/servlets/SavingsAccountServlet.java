package com.hsbc.da1.servlets;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hsbc.da1.model.SavingsAccount;
import com.hsbc.da1.service.SavingsAccountService;

public class SavingsAccountServlet extends HttpServlet {
	
	
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String accountName = req.getParameter("name");
		String accountAmountStr = req.getParameter("amount");
		String accountEmailStr = req.getParameter("email");
		
		double amount = Double.parseDouble(accountAmountStr);
		SavingsAccount savingaccount = new SavingsAccount(accountName, amount , accountEmailStr);
		SavingsAccountService saservice = SavingsAccountService.getSavingsAccountService();
		SavingsAccount newsavingsaccountEntry = saservice.createSavingsAccount(savingaccount);
		req.setAttribute("savingsaccount", newsavingsaccountEntry);
		RequestDispatcher rd = req.getRequestDispatcher("FetchById.jsp");
		rd.forward(req, res);
		//doGet(req,res);
	}

	
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) {
		SavingsAccountService saservice = SavingsAccountService.getSavingsAccountService();
		List<SavingsAccount> sa = saservice.fetchSavingsAccounts();
		
		for(SavingsAccount index: sa) {
			System.out.println(index);
		}
	}

}