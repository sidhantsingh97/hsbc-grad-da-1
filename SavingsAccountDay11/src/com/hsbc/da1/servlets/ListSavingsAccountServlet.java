package com.hsbc.da1.servlets;

import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hsbc.da1.model.SavingsAccount;
import com.hsbc.da1.service.SavingsAccountService;

public class ListSavingsAccountServlet extends HttpServlet{

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) {
		SavingsAccountService saservice = SavingsAccountService.getSavingsAccountService();
		List<SavingsAccount> sa = saservice.fetchSavingsAccounts();
		
		for(SavingsAccount index: sa) {
			System.out.println(index);
		}
	}
}
