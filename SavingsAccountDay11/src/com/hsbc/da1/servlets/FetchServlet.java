package com.hsbc.da1.servlets;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hsbc.da1.model.SavingsAccount;
import com.hsbc.da1.service.SavingsAccountService;



public class FetchServlet extends HttpServlet {
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		
		SavingsAccountService saservice = SavingsAccountService.getSavingsAccountService();
		Collection<SavingsAccount> savingsaccount = saservice.fetchSavingsAccounts();
		req.setAttribute("savingsaccount", savingsaccount);
		RequestDispatcher rd = req.getRequestDispatcher("list.jsp");
		rd.forward(req, res);
	}

}
