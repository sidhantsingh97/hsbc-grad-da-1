package com.hsbc.da1.servlets;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hsbc.da1.model.SavingsAccount;
import com.hsbc.da1.service.SavingsAccountService;

public class FetchSavingsAccountById extends HttpServlet{

public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		
		String accountNo = req.getParameter("accountNumber");
		long accountNumber = Long.parseLong(accountNo);		
		SavingsAccountService saservice = SavingsAccountService.getSavingsAccountService();
		SavingsAccount savingsaccount = saservice.fetchSavingsAccountByAccountId(accountNumber);
		req.setAttribute("savingsaccount", savingsaccount);
		RequestDispatcher rd = req.getRequestDispatcher("FetchById.jsp");
		rd.forward(req, res);
	}
	
}
