package com.hsbc.da1.service;

import com.hsbc.da1.model.SavingsAccount;
import com.hsbc.da1.dao.SavingsAccountDAO;
import com.hsbc.da1.exception.CustomerNotFoundException;
import com.hsbc.da1.exception.InsufficientBalanceException;



import java.util.Collection;
import java.util.List;

public final class SavingsAccountServiceImpl implements SavingsAccountService {

	private SavingsAccountDAO dao = SavingsAccountDAO.getInstance();

	public SavingsAccount createSavingsAccount(SavingsAccount savingsaccount) {

		// no validations
		SavingsAccount savingsAccountCreated = this.dao.saveSavingsAccount(savingsaccount);
		return savingsAccountCreated;
	}

	public void deleteSavingsAccount(long accountNumber) {
		this.dao.deleteSavingsAccount(accountNumber);
	}

	public List<SavingsAccount> fetchSavingsAccounts() {
		return this.dao.fetchSavingsAccounts();
		
	}

	public SavingsAccount fetchAccountByPIN(int pin) {
		return null;
	}

	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) {
		SavingsAccount savingsAccount = null;
		try {
			 savingsAccount = this.dao.fetchSavingsAccountByAccountId(accountNumber);
		} catch (CustomerNotFoundException e) {
			
		}
		return savingsAccount;
	}

	public double withdraw(long accountId, double amount) throws InsufficientBalanceException, CustomerNotFoundException  {
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountByAccountId(accountId);
		if (savingsAccount != null) {
			double currentAccountBalance = savingsAccount.getAccountBalance();
			if (currentAccountBalance >= amount) {
				currentAccountBalance = currentAccountBalance - amount;
				savingsAccount.setAccountBalance(currentAccountBalance);
				this.dao.updateSavingsAccount(accountId, savingsAccount);
				return amount;
			} else {
				throw new InsufficientBalanceException("Do not have sufficient balance");
			}
		}
		return 0;
	}

	public double deposit(long accountId, double amount) throws CustomerNotFoundException {
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountByAccountId(accountId);
		if (savingsAccount != null) {
			double currentAccountBalance = savingsAccount.getAccountBalance();
			savingsAccount.setAccountBalance(currentAccountBalance + amount);
			this.dao.updateSavingsAccount(accountId, savingsAccount);
			return savingsAccount.getAccountBalance();
		}
		return 0;
	}

	public double checkBalance(long accountId) throws CustomerNotFoundException {
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountByAccountId(accountId);
		if (savingsAccount != null) {
			return savingsAccount.getAccountBalance();
		}
		return 0;
	}

	public void transfer(long accountId, long toId, double amount) throws InsufficientBalanceException, CustomerNotFoundException {
		SavingsAccount fromAccount = this.dao.fetchSavingsAccountByAccountId(accountId);
		SavingsAccount toAccount = this.dao.fetchSavingsAccountByAccountId(toId);
		double updatedBalance = this.withdraw(fromAccount.getAccountNumber(), amount);
		if (updatedBalance != 0) {
			this.deposit(toAccount.getAccountNumber(), amount);
		}
	}

	@Override
	public SavingsAccount updateAccount(String customerName, double accountBalance, String emailAddress,
			long accountNumber) throws CustomerNotFoundException {
		SavingsAccount sa = new SavingsAccount(customerName, accountBalance, emailAddress);
		SavingsAccount saUpdatedAccount = this.dao.updateSavingsAccount(accountNumber, sa);
		return saUpdatedAccount;
	}

}
