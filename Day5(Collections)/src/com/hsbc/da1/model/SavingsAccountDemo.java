package com.hsbc.da1.model;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SavingsAccountDemo {

	public static void main(String[] args) {
		
		SavingsAccount si = new SavingsAccount("Sidhant Singh", 3000);
		si.setAccountNumber(1234);
		
		SavingsAccount sa = new SavingsAccount("Sayan Muko", 6000);
		sa.setAccountNumber(1234);
		
		Set<SavingsAccount> savingsAccountSet = new HashSet<>();
		
		savingsAccountSet.add(si);
		savingsAccountSet.add(sa);
		
		SavingsAccount ranu = new SavingsAccount("Ranu Sharma",7000);
		ranu.setAccountNumber(12345786);
		savingsAccountSet.add(ranu);
		
		Iterator<SavingsAccount> it = savingsAccountSet.iterator();
		
		while(it.hasNext())
		{
			System.out.println(it.next());
		}
		
	}
}
