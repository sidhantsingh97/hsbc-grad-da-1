package com.hsbc.da1.set;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class HashSetDemo {

	Set<Integer> set = new HashSet<>();
	set.add(23);
	set.add(23);
	set.add(23);
	set.add(23);
	set.add(63);
	set.add(47);
	set.add(55);
	
	System.out.println("Total number of elements %d %n", set.size());
	System.out.println(set.contains(23));
	
	Iterator<Integer> it = set.iterator();
	
	while(it.hasNext())
	{
		int value = it.next();
		System.out.println("The value is "+ value);
	}
	
	System.out.println("Size of the set :"+set.size());
}
