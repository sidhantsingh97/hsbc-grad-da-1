package com.hsbc.da1.dao;

import java.util.List;

import com.hsbc.da1.exception.UserNotFoundException;
import com.hsbc.da1.model.User;

public interface UserDAO {
	
	Boolean saveUser(User user);
	
	User updateUser(long accountNumber, User User) throws UserNotFoundException;
	
	void deleteUser(long accountNumber);
	
	List<User> fetchUsers();
	
	User fetchUserByUserName(String  user) throws UserNotFoundException;
		
	User fetchUserByAccountId(long accountNumber) throws UserNotFoundException;
	
	static UserDAO getInstance() {
		return new JdbcBackedDAOImpl();
	}

}
