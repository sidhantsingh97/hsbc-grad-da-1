package com.hsbc.da1.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.time.LocalDate;
import com.hsbc.da1.exception.UserNotFoundException;
import com.hsbc.da1.model.User;

public class JdbcBackedDAOImpl implements UserDAO{
	
	private static String connectString = "jdbc:derby://localhost:1527/siddb";
	private static String username = "admin";
	private static String password = "password";
	
	private static final String INSERT_QUERY_PREP_STMT = "insert into User_Table (user_name, user_phone, user_password,Date_of_Birth)"
			+ " values ( ?, ?, ? , ?)";
	
	private static final String SELECT_QUERY = "select * from User_Table";
	
	private static final String DELETE_BY_ID_QUERY = "delete from User_Table where user_id=?";
	
	private static final String SELECT_BY_ID_QUERY = "select *  from User_Table where user_id=?";
	
	private static final String SELECT_BY_NAME_QUERY = "select *  from User_Table where user_name=?";
	
	private static final String UPDATE_BY_ID_QUERY = "update savings_account set user_name = ? ,user_phone= ?, user_password = ?, Date_of_Birth = ? where user_id = ?";
	
	private static Connection getDBConnection() {
		try {
			Class.forName("org.apache.derby.jdbc.ClientDriver");
			Connection connection = DriverManager.getConnection(connectString, username, password);
			return connection;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		catch(ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Boolean saveUser(User user) {
		
		int numberOfRecordsUpdated = 0;
		Boolean b = false;
		try (PreparedStatement pStmt = getDBConnection().prepareStatement(INSERT_QUERY_PREP_STMT);) {
			pStmt.setString(1, user.getUserName());
			pStmt.setString(2, user.getUserPhone());
			pStmt.setString(3, user.getUserPassword());
			pStmt.setDate(4, Date.valueOf(user.getUserDateOfBirth()));

			numberOfRecordsUpdated = pStmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (numberOfRecordsUpdated == 1) {
			
				 b = true;
		}

			return b;
		
}

	@Override
	public User updateUser(long userId, User user) throws UserNotFoundException {
		// TODO Auto-generated method stub
		
		
		
		
		
		return null;
	}

	@Override
	public void deleteUser(long accountNumber) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<User> fetchUsers() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public User fetchUserByAccountId(long accountNumber) throws UserNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User fetchUserByUserName(String userName) throws UserNotFoundException {	
		try {
			Connection con = getDBConnection();
			PreparedStatement psmt = con.prepareStatement(SELECT_BY_NAME_QUERY);
			psmt.setString(1, userName);
			ResultSet rs = psmt.executeQuery();
			if (rs.next()) {
				LocalDate localDate = rs.getDate("Date_of_Birth").toLocalDate();
				User user = new User(rs.getString("user_name"),rs.getString("user_password"),rs.getString("user_phone"),localDate,rs.getLong("user_id"));
				return user;
			}
				throw new UserNotFoundException(" Customer with " + userName + " does not exists");
		} catch (SQLException e) {
			e.printStackTrace();
			
		}
		return null;
	}
	
	

}
