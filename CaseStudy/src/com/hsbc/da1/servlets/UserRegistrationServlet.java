package com.hsbc.da1.servlets;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hsbc.da1.model.User;
import com.hsbc.da1.service.UserService;

public class UserRegistrationServlet extends HttpServlet {

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String userName = req.getParameter("name");
		String userPhoneNumber = req.getParameter("phone");
		String userPassword = req.getParameter("password");
		String userDOB = req.getParameter("dob");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		LocalDate userDateOfBirth = LocalDate.parse(userDOB, formatter);
		User user = new User(userName, userPassword, userPhoneNumber, userDateOfBirth);
		UserService userService = UserService.getUserService();
		Boolean bool = userService.saveUser(user);

		if (bool) {
			res.sendRedirect("RegistrationSuccessfull.jsp");
		} else {
			res.sendRedirect("RegistrationError.jsp");
		}
	}
	// doGet(req,res);
}
