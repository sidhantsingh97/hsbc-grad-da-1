package com.hsbc.da1.servlets;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hsbc.da1.model.User;
import com.hsbc.da1.service.UserService;

public class UserLoginServlet extends HttpServlet {

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String userName = req.getParameter("name");
		String userPassword = req.getParameter("password");
		UserService userService = UserService.getUserService();
		Boolean bool = userService.userLogin(userName, userPassword);
		//System.out.println(bool);
		if (bool) {
			res.sendRedirect("LoginSuccessfull.jsp");
		} else {
			res.sendRedirect("RegistrationSuccessfull.jsp");
		}
	}
	
}
