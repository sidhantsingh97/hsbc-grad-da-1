package com.hsbc.da1.model;

import java.time.LocalDate;

public class User {

	private String userName;
	
	private String userPassword;
	
	private String userPhone;
	
	private LocalDate userDateOfBirth;
	
	private long userId;

	public User(String userName, String userPassword, String userPhone, LocalDate userDateOfBirth) {
		super();
		this.userName = userName;
		this.userPassword = userPassword;
		this.userPhone = userPhone;
		this.userDateOfBirth = userDateOfBirth;
	}

	public User(String userName, String userPassword, String userPhone, LocalDate userDateOfBirth, long userId) {
		super();
		this.userName = userName;
		this.userPassword = userPassword;
		this.userPhone = userPhone;
		this.userDateOfBirth = userDateOfBirth;
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public LocalDate getUserDateOfBirth() {
		return userDateOfBirth;
	}

	public void setUserDateOfBirth(LocalDate userDateOfBirth) {
		this.userDateOfBirth = userDateOfBirth;
	}

	public long getUserId() {
		return userId;
	}

	@Override
	public String toString() {
		return "Contact [userName=" + userName + ", userPassword=" + userPassword + ", userPhone=" + userPhone
				+ ", userDateOfBirth=" + userDateOfBirth + ", userId=" + userId + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (userId ^ (userId >>> 32));
		result = prime * result + ((userName == null) ? 0 : userName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (userId != other.userId)
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}
	
	
	
}
