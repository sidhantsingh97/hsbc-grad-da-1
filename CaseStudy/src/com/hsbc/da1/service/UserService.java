package com.hsbc.da1.service;

import com.hsbc.da1.exception.UserNotFoundException;
import com.hsbc.da1.model.User;

public interface UserService {

	Boolean saveUser(User user);
	
	static UserService getUserService() {
		return new UserServiceImpl();
	}
	
	Boolean  userLogin(String UserName,String password);
}
