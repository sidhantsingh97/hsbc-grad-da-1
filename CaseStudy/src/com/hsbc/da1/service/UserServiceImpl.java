package com.hsbc.da1.service;

import com.hsbc.da1.dao.UserDAO;
import com.hsbc.da1.exception.UserNotFoundException;
import com.hsbc.da1.model.User;

public class UserServiceImpl implements UserService {
	
	private UserDAO dao = UserDAO.getInstance();

	@Override
	public Boolean saveUser(User user) {
		
		Boolean b = this.dao.saveUser(user);
		return b;
	}

	@Override
	public Boolean userLogin(String UserName, String password) {
		
		Boolean bool = false;
		try {
			User user = this.dao.fetchUserByUserName(UserName);
			System.out.println(user.getUserPassword());
			System.out.println(password);
			if(user.getUserPassword().equals(password))
			{
				bool = true;
			}
		} catch (UserNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		System.out.println(bool);
		return bool;
	}
	
	
}
