package com.hsbc.da1.model;

public class Item {

	private String itemName;
	
	private static int counter = 1000;
	
	private long itemId;
	
	private double itemPrice;
	
	public Item(String itemName,double itemPrice)
	{
		this.itemName = itemName;
		this.itemPrice = itemPrice;
		this.itemId = ++counter;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public double getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(double itemPrice) {
		this.itemPrice = itemPrice;
	}

	public long getitemId() {
		return itemId;
	}
}
