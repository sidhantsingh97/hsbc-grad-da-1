package com.hsbc.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AgeCalculatorServlet extends HttpServlet{
	
	public void doGet(HttpServletRequest httpServletReq, HttpServletResponse httpServletRes) throws IOException {
		
		
		
		LocalDate currentDate  = LocalDate.now();
		
		String dobStr = httpServletReq.getParameter("dob");
		
		//calculate the age in days
		//int days =Integer.parseInt(dobStr);
		
		LocalDate dob = LocalDate.parse(dobStr, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
		
		PrintWriter out = httpServletRes.getWriter();
		
		out.write("<h1> You are :"+ChronoUnit.DAYS.between(dob, currentDate)+" days old!! </h1>");
		
		
		
	}

}
