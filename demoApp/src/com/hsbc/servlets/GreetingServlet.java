package com.hsbc.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GreetingServlet  extends HttpServlet  {

	@Override
	public void init() {
		System.out.println(" Inside the init method of the servlet");
	}

	@Override
	public void doGet(HttpServletRequest httpServletReq, HttpServletResponse httpServletRes) throws IOException {
		
		String firstName = httpServletReq.getParameter("firstname");
		String lastName = httpServletReq.getParameter("lastname");
		
		LocalDateTime currentDate = LocalDateTime.now();
		
		PrintWriter out = httpServletRes.getWriter();
		
		out.write("<h1> Welcome :"+firstName+ " " +lastName+ ": Current date: "+currentDate.toString()+" </h1>");
	}
}
