package com.hsbc.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HelloWorldServlet extends HttpServlet {

	@Override
	public void init() {
		System.out.println(" Inside the init method of the servlet");
	}

	@Override
	public void doGet(HttpServletRequest httpServletReq, HttpServletResponse httpServletRes)
			throws IOException, ServletException {

		LocalDateTime currentDate = LocalDateTime.now();

		if (httpServletReq.getParameter("firstname") != null && 
				httpServletReq.getParameter("lastname") != null) {
			RequestDispatcher rd = httpServletReq.getRequestDispatcher("/greeting");
			rd.forward(httpServletReq, httpServletRes);
		} else {
			httpServletRes.sendRedirect("age");
			
		}

		/*
		 * PrintWriter out = httpServletRes.getWriter();
		 * 
		 * out.write("<h1> The current Date is: " +currentDate.toString()+" </h1>");
		 */
	}
}
