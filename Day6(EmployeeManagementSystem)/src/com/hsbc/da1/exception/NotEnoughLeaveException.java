package com.hsbc.da1.exception;

public class NotEnoughLeaveException extends Exception {
	
	public NotEnoughLeaveException(String message) {
		super(message);
	}

	public String getMessage() {
		return super.getMessage();
	}
}
