package com.hsbc.da1.util;

import com.hsbc.da1.service.*;

public class EmployeeManagementSystemServiceFactory {
	
	public static EmployeeManagementSystemService getInstance () {
		return new EmployeeManagementSystemServiceImpl();
	}

}