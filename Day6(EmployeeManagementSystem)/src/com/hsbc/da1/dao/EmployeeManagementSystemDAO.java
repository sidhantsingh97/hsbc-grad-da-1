package com.hsbc.da1.dao;

import java.util.Collection;
import java.util.List;

//import com.hsbc.da1.exception.CustomerNotFoundException;
import com.hsbc.da1.model.*;

public interface EmployeeManagementSystemDAO {
	EmployeeManagementSystem saveEmployee(EmployeeManagementSystem EmployeeManagementSystem);
	
	EmployeeManagementSystem updateEmployee(long accountNumber, EmployeeManagementSystem EmployeeManagementSystem);
	
	void deleteEmployee(long accountNumber);
	
	Collection<EmployeeManagementSystem> fetchEmployee();
	
	
	EmployeeManagementSystem fetchEmployeeByEmployeeId(long accountNumber);// throws CustomerNotFoundException;
	
	EmployeeManagementSystem fetchEmployeeByEmployeeName(String employeeName);// throws CustomerNotFoundException;
	

}