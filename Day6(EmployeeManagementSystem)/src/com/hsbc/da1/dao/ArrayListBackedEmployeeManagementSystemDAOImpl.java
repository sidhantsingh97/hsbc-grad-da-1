package com.hsbc.da1.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import com.hsbc.da1.model.EmployeeManagementSystem;
import com.hsbc.da1.model.EmployeeManagementSystem;

public class ArrayListBackedEmployeeManagementSystemDAOImpl implements EmployeeManagementSystemDAO{
	
	private List<EmployeeManagementSystem> employeeList = new ArrayList<>();

	@Override
	public EmployeeManagementSystem saveEmployee(EmployeeManagementSystem employee) {
		this.employeeList.add(employee);
		return employee;
	}

	@Override
	public EmployeeManagementSystem updateEmployee(long employeeNumbe, EmployeeManagementSystem employee) {
			for(EmployeeManagementSystem sa: employeeList) {
				if (sa.getEmployeeNumber() == employeeNumbe) {
					sa = employee;
				}
			}
			return employee;
	}

	@Override
	public void deleteEmployee(long employeeNumber) {
		for(EmployeeManagementSystem sa: employeeList) {
			if (sa.getEmployeeNumber() == employeeNumber) {
				this.employeeList.remove(sa);
			}
		}
		
	}

	@Override
	public Collection<EmployeeManagementSystem> fetchEmployee() {
		return this.employeeList;
	}

	@Override
	public EmployeeManagementSystem fetchEmployeeByEmployeeId(long employeeNumber) {
		for(EmployeeManagementSystem sa: employeeList) {
			if (sa.getEmployeeNumber() == employeeNumber) {
				return sa;
			}
		}
		return null;
	}
	
	@Override
	public EmployeeManagementSystem fetchEmployeeByEmployeeName(String employeeName) {
		for(EmployeeManagementSystem sa: employeeList) {
			if (sa.getEmployeeName().equals(employeeName)) {
				return sa;
			}
		}
		return null;
	}

}
