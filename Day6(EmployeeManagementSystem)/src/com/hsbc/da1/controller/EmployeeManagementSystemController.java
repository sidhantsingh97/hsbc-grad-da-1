package com.hsbc.da1.controller;

import java.util.Collection;

import com.hsbc.da1.exception.NotEnoughLeaveException;
import com.hsbc.da1.model.EmployeeManagementSystem;
import com.hsbc.da1.service.*;
import com.hsbc.da1.util.*;

public class EmployeeManagementSystemController {
	
	private EmployeeManagementSystemService employeeService = EmployeeManagementSystemServiceFactory.getInstance();
	
	public EmployeeManagementSystem openEmployee(String employeeName, double employeeBalance) {
		
		EmployeeManagementSystem employeems = this.employeeService.createEmployee(employeeName, employeeBalance);
		return employeems;
	}

	public void deleteEmployee(long employeeNumber) {
		this.employeeService.deleteEmployee(employeeNumber);
		
	}


	public Collection<EmployeeManagementSystem> fetchEmployee() {
		return this.employeeService.fetchEmployee();
	}


	public EmployeeManagementSystem fetchEmployeeByemployeeId(long employeeNumber) {
		EmployeeManagementSystem employeems = this.employeeService.fetchEmployeeByemployeeId(employeeNumber);
		return employeems;
	}
	
	public EmployeeManagementSystem fetchEmployeeByemployeeName(String employeeName) {
		EmployeeManagementSystem employeems = this.employeeService.fetchEmployeeByEmployeeName(employeeName);
		return employeems;
	}

	public int checkLeave(long employeeNumber) {
		int currentLeave = this.employeeService.checkLeave(employeeNumber);
		return currentLeave;
	}

	public int applyForLeave(long employeeNumber, int noOfLeave)throws NotEnoughLeaveException  {
	
		int leaveGranted = this.employeeService.applyForLeave(employeeNumber, noOfLeave);
		return leaveGranted;	
	}
	

}
