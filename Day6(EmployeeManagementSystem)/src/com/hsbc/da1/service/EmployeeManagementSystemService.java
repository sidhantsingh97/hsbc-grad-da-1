package com.hsbc.da1.service;

import java.util.Collection;
import java.util.List;

import com.hsbc.da1.exception.NotEnoughLeaveException;
import com.hsbc.da1.model.*;

public interface EmployeeManagementSystemService {

	public EmployeeManagementSystem createEmployee(String employeerName, double employeeBalance);

	public void deleteEmployee(long employeeNumber);

	public Collection<EmployeeManagementSystem> fetchEmployee();

	public EmployeeManagementSystem fetchEmployeeByemployeeId(long employeeNumber);

	public int checkLeave(long employeeNumber);// throws CustomerNotFoundException;
	
	public int applyForLeave(long employeeNumber,int noOfLeave)throws NotEnoughLeaveException;

	public EmployeeManagementSystem fetchEmployeeByEmployeeName(String employeeName);

}