package com.hsbc.da1.service;

import com.hsbc.da1.util.*;

import java.util.Collection;

import com.hsbc.da1.dao.EmployeeManagementSystemDAO;
import com.hsbc.da1.exception.NotEnoughLeaveException;
import com.hsbc.da1.model.*;

public class EmployeeManagementSystemServiceImpl implements EmployeeManagementSystemService {
	
	private EmployeeManagementSystemDAO dao = EmployeeManagementSystemDAOFactory.getEmployeeManagementSystemDAO(1);

	@Override
	public EmployeeManagementSystem createEmployee(String employeeName, double employeeBalance) {
		EmployeeManagementSystem employeems = new EmployeeManagementSystem(employeeName, employeeBalance);

		EmployeeManagementSystem employeemsCreated = this.dao.saveEmployee(employeems);
		return employeemsCreated;
	}

	@Override
	public void deleteEmployee(long employeeNumber) {
		this.dao.deleteEmployee(employeeNumber);
		
	}

	@Override
	public Collection<EmployeeManagementSystem> fetchEmployee() {
		return this.dao.fetchEmployee();
	}

	@Override
	public EmployeeManagementSystem fetchEmployeeByemployeeId(long employeeNumber) {
		EmployeeManagementSystem employeems = this.dao.fetchEmployeeByEmployeeId(employeeNumber);
		return employeems;
	}
	
	@Override
	public int checkLeave(long employeeNumber) {
		EmployeeManagementSystem employeems = this.dao.fetchEmployeeByEmployeeId(employeeNumber);
		if(employeems!=null)
		{
			int currentLeave = employeems.getEmployeeLeave();
			return currentLeave;
		}
		return 0;
	}

	@Override
	public int applyForLeave(long employeeNumber, int noOfLeave)throws NotEnoughLeaveException {
	
		EmployeeManagementSystem employeems = this.dao.fetchEmployeeByEmployeeId(employeeNumber);
		if(employeems!=null)
		{
			if(noOfLeave<=10)
			{
				int currentLeave = employeems.getEmployeeLeave();
				if((currentLeave-noOfLeave)>0)
				{
					currentLeave-=noOfLeave;
					employeems.setEmployeeLeave(currentLeave);
					this.dao.updateEmployee(employeeNumber, employeems);
					return noOfLeave;
				}
				else
				{
					throw new NotEnoughLeaveException("You don't have enough leaves..");
				}
			}
			else
			{
				System.out.println("Enter leave less than or equal to 10");
			}
		}
		return 0;		
	}

	@Override
	public EmployeeManagementSystem fetchEmployeeByEmployeeName(String employeeName) {
		EmployeeManagementSystem employeems = this.dao.fetchEmployeeByEmployeeName(employeeName);
		return employeems;
	}

}
