package com.hsbc.da1.model;

public class EmployeeManagementSystem implements Comparable<EmployeeManagementSystem>{
	
	private String employeeName;
	
	private long employeeNumber;
	
	private double employeeBalance;
	
	private int employeeAge;
	
	private int employeeLeave;
	
	private static long counter = 1000;
	
	public EmployeeManagementSystem(String employeeName, double employeeBalance) {
		this.employeeName = employeeName;
		this.employeeBalance = employeeBalance;
		this.employeeLeave = 40;
		this.employeeNumber = ++ counter;
	}
	
	public EmployeeManagementSystem(String employeeName, double employeeBalance,int employeeAge) {
		this.employeeName = employeeName;
		this.employeeBalance = employeeBalance;
		this.employeeLeave = 40;
		this.employeeAge = employeeAge;
		this.employeeNumber = ++ counter;
	}

	
	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public double getEmployeeBalance() {
		return employeeBalance;
	}

	public void setEmployeeBalance(double employeeBalance) {
		this.employeeBalance = employeeBalance;
	}

	public int getEmployeeAge() {
		return employeeAge;
	}

	public void setEmployeeAge(int employeeAge) {
		this.employeeAge = employeeAge;
	}

	public long getEmployeeNumber() {
		return employeeNumber;
	}
	
	public int getEmployeeLeave() {
		return employeeLeave;
	}

	public void setEmployeeLeave(int employeeLeave) {
		this.employeeLeave = employeeLeave;
	}

	


	@Override
	public String toString() {
		return "EmployeeManagementSystem [employeeName=" + employeeName + ", employeeNumber=" + employeeNumber
				+ ", employeeBalance=" + employeeBalance + ", employeeAge=" + employeeAge + ", employeeLeave="
				+ employeeLeave + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((employeeName == null) ? 0 : employeeName.hashCode());
		result = prime * result + (int) (employeeNumber ^ (employeeNumber >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmployeeManagementSystem other = (EmployeeManagementSystem) obj;
		if (employeeName == null) {
			if (other.employeeName != null)
				return false;
		} else if (!employeeName.equals(other.employeeName))
			return false;
		if (employeeNumber != other.employeeNumber)
			return false;
		return true;
	}

	@Override
	public int compareTo(EmployeeManagementSystem account) {
	 	 //return (int)(this.employeeBalance - account.getemployeeBalance());
		return -1 * account.employeeName.compareTo(this.employeeName);
	}
}
