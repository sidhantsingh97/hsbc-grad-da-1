package com.hsbc.da1.client;

import java.util.Collection;

import com.hsbc.da1.controller.*;
import com.hsbc.da1.exception.NotEnoughLeaveException;
import com.hsbc.da1.model.* ;

public class EmployeeManagementSystemClient {
	
	public static void main(String[] args) {
		
		EmployeeManagementSystemController empcontroller = new EmployeeManagementSystemController();
		System.out.println("------------------------------------------------------------------");
		System.out.println("                  Let us open two Employee details.                ");
		
		EmployeeManagementSystem sidhant = empcontroller.openEmployee("Sidhant Singh", 3000);
		EmployeeManagementSystem ranu = empcontroller.openEmployee("Ranu", 6000);
		System.out.println("                  Employee Created                        ");
		System.out.println("------------------------------------------------------------------");
		
		System.out.println("---------------------------CHECK BALANCE--------------------------------");
		System.out.println("Account Balance of "+sidhant.getEmployeeName()+" initially is :"+sidhant.getEmployeeBalance());
		System.out.println("Account Balance of "+ranu.getEmployeeName()+" initially is :"+ranu.getEmployeeBalance());
		System.out.println("------------------------------------------------------------------\n\n");
		
		System.out.println("---------------------------CHECK LEAVE --------------------------------");
		System.out.println("Leave Balance of "+sidhant.getEmployeeName()+" initially is :"+empcontroller.checkLeave(1001));
		System.out.println("Leave Balance of "+ranu.getEmployeeName()+" initially is :"+empcontroller.checkLeave(1002));
		System.out.println("------------------------------------------------------------------\n\n");

		System.out.println("---------------------------APPLY FOR LEAVE--------------------------------");
		try {
		System.out.println("Leave got after applying is for "+sidhant.getEmployeeName()+" is "+empcontroller.applyForLeave(1001, 10));
		System.out.println("Leave got after applying is for "+sidhant.getEmployeeName()+" is "+empcontroller.applyForLeave(1001, 10));
		System.out.println("Leave got after applying is for "+sidhant.getEmployeeName()+" is "+empcontroller.applyForLeave(1001, 10));
		System.out.println("Leave got after applying is for "+sidhant.getEmployeeName()+" is "+empcontroller.applyForLeave(1001, 8));
		System.out.println("Leave got after applying is for "+sidhant.getEmployeeName()+" is "+empcontroller.applyForLeave(1001, 5));
		System.out.println("Leave got after applying is for "+ranu.getEmployeeName()+" is "+empcontroller.applyForLeave(1002, 10));
		}catch(NotEnoughLeaveException exception) {
			System.out.println(exception.getMessage());
		}
		System.out.println("------------------------------------------------------------------\n\n");
		
		System.out.println("-----------------PRINTING ALL THE DETAILS OF ALL THE EMPLOYEES--------------------");
		Collection<EmployeeManagementSystem> employeems = empcontroller.fetchEmployee();
		for(EmployeeManagementSystem emp : employeems)
		{
			if(emp==null)
				break;
			else
			{
				System.out.println("\n"+emp+"\n");
			}
		}
		System.out.println("------------------------------------------------------------------\n\n");
}
}
