abstract class BankAccount {
    public String name;
    protected double balance;
    private final long accountNumber;
    private static long counter = 1000;

    public BankAccount(String name, double balance) {
        this.name = name;
        this.balance = balance;
        this.accountNumber = ++counter;
    }

    public final void deposit(double amount) {
        this.balance += amount;
    }

    public final String getCustomerDetails() {
        return "Customer name: " + this.name + " Balance: " + this.balance + "  A/C No: " + this.accountNumber;
    }

    public abstract double withdraw(double amount);

    public abstract double getLoanAmount(double loanAmount);

    public abstract double loanElibibilty();

    public static void method1() {
        System.out.println("Inside the parent class");
    }
}

final class CurrentAccount extends BankAccount {
    private String gstNumber;
    //private String name;
    private final long LOAN_ELIGIBILITY = 25_00_000;

    public CurrentAccount(double balance, String name, String gstNumber) {
        super(name, balance);
        this.gstNumber = gstNumber;
        //this.name = name;
        //super.name = name;
    }

    public final double withdraw(double amount) {
        if((this.balance-amount)>=25000){
        this.balance-=amount;
        return amount;
        }
        return 0;
    }

    public final double getLoanAmount(double loanAmount) {
        // validate the loan eligibility
        if(loanAmount<=2500000)
        {
        super.deposit(loanAmount);
        return loanAmount;
        }
        else
        {
            System.out.println("The eligible loan limit is :"+loanElibibilty());
            return 0;
        }
    }

    public final double loanElibibilty() {
        return LOAN_ELIGIBILITY;
    }

}

final class SavingsAccount extends BankAccount{

    private final long LOAN_ELIGIBILITY = 5_00_000;

    public SavingsAccount(double balance, String name) {
        super(name, balance);
    }

    public final double withdraw(double amount) {
        if((amount<=10000) && ((this.balance-amount)>=10000)){
        this.balance-=amount;
        return amount;
        }
        return 0;
    }

    public final double getLoanAmount(double loanAmount) {
        // validate the loan eligibility
        if(loanAmount<=500000)
        {
        super.deposit(loanAmount);
        return loanAmount;
        }
        else
        {
            System.out.println("The eligible loan limit is :"+loanElibibilty());
            return 0;
        }
    }

    public final double loanElibibilty() {
        return LOAN_ELIGIBILITY;
    }

}

final class SalaryAccount extends BankAccount{

    private final long LOAN_ELIGIBILITY = 10_00_000;

    public SalaryAccount(double balance, String name) {
        super(name, balance);
    }

    public final double withdraw(double amount) {
        if((amount<=15000) && ((this.balance-amount)>=0)){
        this.balance-=amount;
        return amount;
        }
        return 0;
    }

    public final double getLoanAmount(double loanAmount) {
        // validate the loan eligibility
        if(loanAmount<=1000000)
        {
        super.deposit(loanAmount);
        return loanAmount;
        }
        else
        {
            System.out.println("The eligible loan limit is :"+loanElibibilty());
            return 0;
        }
    }

    public final double loanElibibilty() {
        return LOAN_ELIGIBILITY;
    }

}

public final class BankAccountClient {

    public static void main(String[] args) {
        String value = args[0];
        BankAccount account=null;

        switch (value) {
            case "1":
                account = new CurrentAccount(28000, "Sidhant Singh", "sdfsdds");
                System.out.println("---------------CURRENT ACCOUNT--------------------\n");
                break;
            case "2":
                account = new SavingsAccount(12000, "Sidhant Singh");
                System.out.println("---------------SAVINGS ACCOUNT--------------------\n");
                break;
            case "3":
                account = new SalaryAccount(12000, "Sidhant Singh");
                System.out.println("---------------SALARY ACCOUNT--------------------\n");
                break;
            default:
                System.out.println("Enter a valid account choice :\n 1.->Current Account\n2.->savings Account\n3.->Salaried Account");
                break;
        }
        System.out.println(account.getCustomerDetails()+"\n");
        System.out.println("Amount withdrawn is : "+account.withdraw(2000)+"\n");
        System.out.println("----------------------------------------------------------------------\n");
        System.out.println(account.getCustomerDetails()+"\n");
        System.out.println("The loan amount availed is :"+account.getLoanAmount(500000));
        System.out.println("----------------------------------------------------------------------\n");
        System.out.println(account.getCustomerDetails()+"\n");
    }

}