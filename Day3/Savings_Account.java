public class Savings_Account {

    // static variables
    private static long accountNumberTracker = 1000;

    // instance variables
    private long accountNumber;

    private double accountBalance;

    private String customerName;

    private String emailAddress;

    private String nominee;

    private String nationality;

    public long getAccountNumber() {
        return accountNumber;
    }

    public Savings_Account(String customerName, double initialAccountBalance) {
        this.customerName = customerName;
        this.accountBalance = initialAccountBalance;
        this.nationality = "INDIAN";
        this.accountNumber = ++accountNumberTracker;
    }
    // instance methods
    public double withdraw(double amount) {
        if (this.accountBalance >= amount) {
            this.accountBalance = this.accountBalance - amount;
            return amount;
        }
        return 0;
    }

    public double checkBalance() {
        return this.accountBalance;
    }

    public double deposit(double amount) {
        this.accountBalance = accountBalance + amount;
        return accountBalance;
    }

    public double deposit(double amount, Savings_Account user) {
        user.accountBalance = user.accountBalance + amount;
        return accountBalance;
    }

    public double deposit(double amount, String notes) {
        this.accountBalance = accountBalance + amount;
        System.out.println("notes: " + notes);
        return accountBalance;
    }

    public String getCustomerName() {
        return this.customerName;

    }

    public static long getCurrentCounterValue() {
        return accountNumberTracker;
    }
}
