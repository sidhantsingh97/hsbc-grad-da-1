//Interface Implementation.
interface PaymentGateway {
    
    void pay(String from, String to, double amount, String notes);

}

interface MobileRecharge {

    void rechargeMobile(String phonenumber, double amount);

}

class GooglePay implements PaymentGateway, MobileRecharge {
    public void pay(String from, String to, double amount, String notes) {
        System.out.println("Payment from " + from + " to: " + to + "of Amount " + amount + " Notes : " + notes
                + " Using Google Pay");
    }

    public void rechargeMobile(String phonenumber, double amount) {
        System.out.println("Mobile recharge for " + phonenumber + " with amount  " + amount + "with GooglePay ");
    }

}

class PhonePay implements PaymentGateway, MobileRecharge {
    public void pay(String from, String to, double amount, String notes) {
        System.out.println("Payment from " + from + " to: " + to + "of Amount " + amount + " Notes : " + notes
                + " Using Phone Pay");
    }

    public void rechargeMobile(String phonenumber, double amount) {
        System.out.println("Mobile recharge for " + phonenumber + " with amount  " + amount + " with PhonePay");
    }
}

class JioPay implements PaymentGateway {
    public void pay(String from, String to, double amount, String notes) {
        System.out.println(
                "Payment from " + from + " to: " + to + "of Amount " + amount + " Notes : " + notes + " Using Jio Pay");
    }
}

public class PaymentGatewayClient {
    public static void main(String[] args) {

        PaymentGateway paymentGateway = null;
        MobileRecharge mobileRecharge = null;
        if (args[0] == "1") {
            GooglePay googlePay = new GooglePay();
            paymentGateway = googlePay;
            mobileRecharge = googlePay;
        } 
        else if(args[0] == "2")
        {
            JioPay jioPay = new JioPay();
            paymentGateway = jioPay;
        }
        else {
            PhonePay phonePay = new PhonePay();
            paymentGateway = phonePay;
            mobileRecharge = phonePay;
        }
        paymentGateway.pay("Nikhil", "Kiran", 15_000, "Please confirm");
        if(mobileRecharge != null){
        mobileRecharge.rechargeMobile("9877784578", 250);
        }
    }
}