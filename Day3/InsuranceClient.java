interface Insurance {
    
    double calculatePremium(double amount,String vehicleName,String model);
    String payPremium(double premiumAmount,String vehicleNumber,String model);

}

class BajajInsurance implements Insurance {
    static int bajajCounter = 1000;
    public double calculatePremium(double amount,String vehicleName,String model)
    {
        double premium =0.25*amount;
        return premium;
    }
    public String payPremium(double premiumAmount,String vehicleNumber,String model)
    {
        String policyNumber="";
        policyNumber = policyNumber.concat("BajajInsurance").concat(vehicleNumber).concat(model)+bajajCounter;
        bajajCounter++;
        return policyNumber;
    }
}

class TataAIG implements Insurance {
    static int tataCounter = 2000;
    public double calculatePremium(double amount,String vehicleName,String model)
    {
        double premium =0.15*amount;
        return premium;
    }
    public String payPremium(double premiumAmount,String vehicleNumber,String model)
    {
        String policyNumber="";
        policyNumber = policyNumber.concat("TataAIG").concat(vehicleNumber).concat(model)+tataCounter;
        tataCounter++;
        return policyNumber;      
    }

}
public class InsuranceClient {
    public static void main(String[] args) {

        String str = args[0];

        Insurance insurance = null;
        if (str.equals("B")) {
            BajajInsurance bajajInsurance = new BajajInsurance();
            insurance = bajajInsurance;
        } 
        else if(str.equals("T"))
        {
            TataAIG tataAIG = new TataAIG();
            insurance = tataAIG;
        }
        else
        System.out.println("Wrong input..");
        double premium = insurance.calculatePremium(60000,"Bolero","GLS220D4MATIC");
        System.out.println("Calculated insurance Premium is:"+ premium);

        Savings_Account savingsaccount = new Savings_Account("Sidhant Singh",10000);

        if(savingsaccount.checkBalance()>=premium)
        {
            savingsaccount.withdraw(premium);
            String policyNo = insurance.payPremium(premium,"670D4","GLS220D4MATIC");
            System.out.println("\nPolicy Number :"+policyNo);

        }
        else
        {
            System.out.println("\nNot enough account Balance");
        }     
    }
}