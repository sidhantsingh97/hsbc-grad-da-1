abstract class Doctor {
    private String name;
    private String specialization;
    private int age;

    public abstract void treatPatient();
}

class Orthopedecian extends Doctor {
    // specialation methods

    public void treatPatient() {
        conductXRay();
        conductCTScan();
    }

    public void conductCTScan() {
        System.out.println("Conducting CT-Scan");
    }

    public void conductXRay() {
        System.out.println("Conducting X-Ray");
    }

}

class Padietrician extends Doctor {

    @Override
    public void treatPatient() {
        treatKids();
    }

    public void treatKids() {
        System.out.println("Treating Kids :)");
    }
}

class Dentist extends Doctor {

    @Override
    public void treatPatient() {
        rootCanal();
    }

    public void rootCanal() {
        System.out.println("Performing root canal operation..");
    }
}

public class DoctorClient {

    public static void main(String[] args) {
        // datatype var = new DataType();
        // Doctor doc = new Doctor();
        // doc.treatPatient();

        Orthopedecian ortho = new Orthopedecian();
        /*
         * ortho.treatPatient(); ortho.conductCTScan(); ortho.conductXRay();
         */

        Doctor doc = null;
        String typeOfDoc = args[0];
        if (typeOfDoc.equals("Ortho")) {
            doc = new Orthopedecian();
        } else if (typeOfDoc == "padietric") {
            doc = new Padietrician();
        } else {
            doc = new Dentist();
        }

        execute(doc);
    }

    public static void execute(Doctor doc) {
        /*
         * if (doc instanceof Orthopedecian) { Orthopedecian orhtopedician =
         * (Orthopedecian) doc; orhtopedician.conductCTScan();
         * orhtopedician.conductXRay(); }
         */
        doc.treatPatient();
    }
}