package com.hsbc.da1.model;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SavingsAccountDemo {
	
	public static void main(String[] args) {
		
		SavingsAccount sa = new SavingsAccount("dinesh", 35000);
		sa.setAccountNumber(1234);

		SavingsAccount dinesh = new SavingsAccount("dinesh", 35000);
		dinesh.setAccountNumber(123454);
		
		Set<SavingsAccount> savingsAccountSet = new HashSet<>();
		
		savingsAccountSet.add(dinesh);
		savingsAccountSet.add(sa);
		
	
		
		SavingsAccount dineddsh = new SavingsAccount("dinesh", 35000);
		dinesh.setAccountNumber(12345489);
		savingsAccountSet.add(dineddsh);
		
		Iterator<SavingsAccount> it = savingsAccountSet.iterator();
		
		while(it.hasNext()) {
			System.out.println(it.next());
		}
		
	}
}
