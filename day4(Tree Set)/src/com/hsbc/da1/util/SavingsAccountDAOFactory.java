package com.hsbc.da1.util;

import com.hsbc.da1.dao.ArrayBackedSaveingsAccountDAOImpl;
import com.hsbc.da1.dao.ArrayListBackedSavingsAccountDAOImpl;
import com.hsbc.da1.dao.SavingsAccountDAO;

public final class SavingsAccountDAOFactory {
	
	private SavingsAccountDAOFactory() {}
	
	public static SavingsAccountDAO getSavingsAccountDAO( int value) {
		if (value == 1 ) {
			return new ArrayBackedSaveingsAccountDAOImpl();
		} else {
			return new ArrayListBackedSavingsAccountDAOImpl();
		}
	}
}
