package com.hsbc.da1.service;

import java.util.Collection;
import java.util.List;

import com.hsbc.da1.exception.CustomerNotFoundException;
import com.hsbc.da1.exception.InsufficientBalanceException;
import com.hsbc.da1.model.SavingsAccount;

public interface SavingsAccountService {

	public SavingsAccount createSavingsAccount(String customerName, double accountBalance);

	public void deleteSavingsAccount(long accountNumber);

	public Collection<SavingsAccount> fetchSavingsAccounts();

	public SavingsAccount fetchAccountByPIN(int pin);

	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber);

	public double withdraw(long accountId, double amount) throws InsufficientBalanceException, CustomerNotFoundException;

	public double deposit(long accountId, double amount) throws CustomerNotFoundException;

	public double checkBalance(long accountId) throws CustomerNotFoundException;

	public void transfer(long accountId, long toId, double amount) throws InsufficientBalanceException, CustomerNotFoundException;
}