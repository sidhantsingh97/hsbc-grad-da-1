public class CurrentAccountClient {

    public static void main(String[] args) {   
        CurrentAccount sidhant = new CurrentAccount("Sidhant","22AABCU9603R1ZX","MyTech");
        System.out.println("Customer Name: " + sidhant.getCustomerName());
        System.out.println("Account Number " + sidhant.getAccountNumber());
        System.out.println("GST Number: " + sidhant.getGstNumber());
        System.out.println("Business name " + sidhant.getBusinessName());
        System.out.println("Initial Account balance " + sidhant.checkBalance());
        System.out.println("Account Balance " + sidhant.deposit(5000));
        System.out.println("Balance after deposit" + sidhant.checkBalance());
        sidhant.withdraw(200);
        System.out.println("Balance after Withdraw " + sidhant.checkBalance());
    }
}