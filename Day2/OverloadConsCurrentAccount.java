public class OverloadConsCurrentAccount {

    //static variables 
    private static long accountNumberTracker = 10000;

    // instance variables
    private long accountNumber;

    private double accountBalance;

    private String customerName;

    private Address address;

    private String emailAddress;

    private String businessName;

    private String gstNumber;

    public long getAccountNumber() {
        return accountNumber;
    }

    public OverloadConsCurrentAccount(String customerName,String gstNumber,String businessName) {
        this.customerName = customerName;
        this.accountBalance=50000;
        this.gstNumber=gstNumber;
        this.businessName=businessName;
        this.accountNumber = ++ accountNumberTracker;
    }

    public OverloadConsCurrentAccount(String customerName, double initialAccountBalance,String gstNumber,String businessName) {
        this.customerName = customerName;
        if(initialAccountBalance>50000)
        this.accountBalance = initialAccountBalance;
        else
        this.accountBalance=50000;
        this.gstNumber=gstNumber;
        this.businessName=businessName;
        this.accountNumber = ++ accountNumberTracker;
    }

    public OverloadConsCurrentAccount(String customerName,String gstNumber,String businessName,Address address) {
        this.customerName = customerName;
        this.accountBalance=50000;
        this.gstNumber=gstNumber;
        this.businessName=businessName;
        this.address=address;
        this.accountNumber = ++ accountNumberTracker;
    }

    public OverloadConsCurrentAccount(String customerName, double initialAccountBalance,String gstNumber,String businessName,Address address) {
        this.customerName = customerName;
        if(initialAccountBalance>50000)
        this.accountBalance = initialAccountBalance;
        else
        this.accountBalance=50000;
        this.gstNumber=gstNumber;
        this.businessName=businessName;
        this.address=address;
        this.accountNumber = ++ accountNumberTracker;
    }

    // instance methods
    public double withdraw(double amount) {
        if ((this.accountBalance > amount) && ((this.accountBalance-amount)>=50000)) {

            this.accountBalance = this.accountBalance - amount;
            return amount;
        }
        return 0;
    }

    public double checkBalance() {
        return this.accountBalance;
    }

    public double deposit(double amount) {
        if(amount>0)
        this.accountBalance = accountBalance + amount;
        return accountBalance;
    }
    public void updateAddress(Address address) {

        this.address = address;
    }

    public String getCustomerName() {
        return this.customerName;
    }

    public String getGstNumber() {
        return this.gstNumber;
    }

    public String getBusinessName() {
        return this.businessName;
    }

}