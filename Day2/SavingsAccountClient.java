
public class SavingsAccountClient {

    public static void main(String[] args) {
        // data type variable name = new data type;
        SavingsAccount rajeesh = new SavingsAccount("Rajeesh", 10_000);
        System.out.println("Customer Name: " + rajeesh.getCustomerName());
        System.out.println("Account Number " + rajeesh.getAccountNumber());
        System.out.println("Initial Account balance " + rajeesh.checkBalance());
        System.out.println("Account Balance " + rajeesh.deposit(5000));
        System.out.println("Balance after deposit" + rajeesh.checkBalance());
        rajeesh.withdraw(200);
        System.out.println("Balance after Withdraw " + rajeesh.checkBalance());

        System.out.println(" ---------------------------------");

        SavingsAccount naveen = new SavingsAccount("Naveen", 50_000, "Disha");
        System.out.println("Account Number " + naveen.getAccountNumber());
        System.out.println("Customer Name: " + naveen.getCustomerName());
        System.out.println("Initial Account balance " + naveen.checkBalance());
        System.out.println("Account Balance " + naveen.deposit(4000));
        System.out.println("Balance after deposit" + naveen.checkBalance());
        naveen.withdraw(2000);
        System.out.println("Balance after Withdraw " + naveen.checkBalance());

        System.out.println(" ---------------------------------");

        Address address = new Address("8th Ave", "Bangalore", "Karnataka", 577142);
        SavingsAccount vinay = new SavingsAccount("Vinay", 34_000, address);
        System.out.println("Customer Name: " + vinay.getCustomerName());

        System.out.println("Initial account Balance of :"+vinay.getCustomerName()+" is :"+vinay.checkBalance());

        vinay.transferAmount(400,1002);

        System.out.println("Final account Balance of :"+vinay.getCustomerName()+" is :"+vinay.checkBalance());
        
        System.out.println("Final account Balance of :"+naveen.getCustomerName()+" is :"+naveen.checkBalance());
        /*System.out.println("---------------------Static Demo ---------------");
        System.out.println("Counter from Rajeesh " + SavingsAccount.getCurrentCounterValue(vinay));
        System.out.println("Counter from Naveen " + SavingsAccount.getCurrentCounterValue(vinay));
        System.out.println("Counter from Vinay " + SavingsAccount.getCurrentCounterValue(vinay));
        System.out.println("---------------------Static Demo ---------------");
        */
    }
}