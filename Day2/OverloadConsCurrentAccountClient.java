public class OverloadConsCurrentAccountClient {

    public static void main(String[] args) {   
        OverloadConsCurrentAccount sidhant = new OverloadConsCurrentAccount("Sidhant","22AABCU9603R1ZX","MyTech");
        System.out.println("Customer Name: " + sidhant.getCustomerName());
        System.out.println("Account Number " + sidhant.getAccountNumber());
        System.out.println("GST Number: " + sidhant.getGstNumber());
        System.out.println("Business name " + sidhant.getBusinessName());
        System.out.println("Initial Account balance " + sidhant.checkBalance());
        System.out.println("Account Balance " + sidhant.deposit(5000));
        System.out.println("Balance after deposit" + sidhant.checkBalance());
        sidhant.withdraw(200);
        System.out.println("Balance after Withdraw " + sidhant.checkBalance());


        System.out.println("-------------------------------------");
        System.out.println("");

        Address address = new Address("8th Ave", "Bangalore", "Karnataka", 577142);
        OverloadConsCurrentAccount snehav = new OverloadConsCurrentAccount("Snehav","22AABCU9603R1ZX","MyTech",address);
        System.out.println("Customer Name: " + snehav.getCustomerName());
        System.out.println("Account Number " + snehav.getAccountNumber());
        System.out.println("GST Number: " + snehav.getGstNumber());
        System.out.println("Business name " + snehav.getBusinessName());
        System.out.println("Initial Account balance " + snehav.checkBalance());
        System.out.println("Account Balance " + snehav.deposit(5000));
        System.out.println("Balance after deposit" + snehav.checkBalance());
        sidhant.withdraw(200);
        System.out.println("Balance after Withdraw " + snehav.checkBalance());
    }
}