public class CallByValRef {

    public static void main(String[] args) {

        int array[] = new int[] { 45, 76, 93, 90 };

        int operand1 = 33;
        int operand2 = 99;

        System.out.println("Call By Value\n");
        System.out.printf("Call by value before call"+operand1+","+operand2);
        
        System.out.println("-------------------------------");
        callByValue(operand1,operand2);

        System.out.printf("Call by value after call"+operand1+","+operand2);

        System.out.println();
        System.out.println();
        System.out.println("Now Call By Reference");


        System.out.printf("Changes before the call");
        for (int a : array) {
            System.out.println(a);
        }
        System.out.println("-------------------------------");
        callByRef(array);

        System.out.printf("Changes before the call");
        for (int a : array) {
            System.out.println(a);
        }
    }

    private static void callByValue(int operand1, int operand2) {
        operand1 = operand1 * 10;
        operand2 = operand2 * 34;

        System.out.printf("Local changes applied  %d %d \n", operand1, operand2);

    }

    private static void callByRef(int[] arr) {
        System.out.println("-------------------------------");
        System.out.print("Changes inside the method");

        arr[0] = 14;
        arr[1] = 25;
        arr[2] = 36;
        arr[3] = 47;
        for (int a : arr) {
            System.out.println(a);
        }

    }
}