public class CurrentAccount {

    //static variables 
    private static long accountNumberTracker = 10000;

    // instance variables
    private long accountNumber;

    private double accountBalance;

    private String customerName;

    private String street;

    private String city;

    private int zipCode;

    private String emailAddress;

    private String businessName;

    private String nationality;

    private String gstNumber;

    public long getAccountNumber() {
        return accountNumber;
    }

    public CurrentAccount(String customerName,String gstNumber,String businessName) {
        this.customerName = customerName;
        this.accountBalance=50000;
        this.gstNumber=gstNumber;
        this.businessName=businessName;
        this.accountNumber = ++ accountNumberTracker;
    }

    public CurrentAccount(String customerName, double initialAccountBalance,String gstNumber,String businessName) {
        this.customerName = customerName;
        if(initialAccountBalance>50000)
        this.accountBalance = initialAccountBalance;
        else
        this.accountBalance=50000;
        this.gstNumber=gstNumber;
        this.businessName=businessName;
        this.accountNumber = ++ accountNumberTracker;
    }

    public CurrentAccount(String customerName,String gstNumber,String businessName,String street, String city, int zipCode,String nationality) {
        this.customerName = customerName;
        this.accountBalance=50000;
        this.gstNumber=gstNumber;
        this.businessName=businessName;
        this.street = street;
        this.city = city;
        this.zipCode = zipCode;
        this.nationality = nationality;
        this.accountNumber = ++ accountNumberTracker;
    }

    public CurrentAccount(String customerName, double initialAccountBalance,String gstNumber,String businessName,String street, String city, int zipCode,String nationality) {
        this.customerName = customerName;
        if(initialAccountBalance>50000)
        this.accountBalance = initialAccountBalance;
        else
        this.accountBalance=50000;
        this.gstNumber=gstNumber;
        this.businessName=businessName;
        this.street = street;
        this.city = city;
        this.zipCode = zipCode;
        this.nationality = nationality;
        this.accountNumber = ++ accountNumberTracker;
    }

    // instance methods
    public double withdraw(double amount) {
        if ((this.accountBalance > amount) && ((this.accountBalance-amount)>=50000)) {

            this.accountBalance = this.accountBalance - amount;
            return amount;
        }
        return 0;
    }

    public double checkBalance() {
        return this.accountBalance;
    }

    public double deposit(double amount) {
        if(amount>0)
        this.accountBalance = accountBalance + amount;
        return accountBalance;
    }
    public void updateAddress(String streetName, int zipCode, String city) {

        this.street = streetName;
        this.zipCode = zipCode;
        this.city = city;
    }

    public String getCustomerName() {
        return this.customerName;
    }

    public String getGstNumber() {
        return this.gstNumber;
    }

    public String getBusinessName() {
        return this.businessName;
    }

}