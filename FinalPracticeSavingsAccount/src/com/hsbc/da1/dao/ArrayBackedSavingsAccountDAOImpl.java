package com.hsbc.da1.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.hsbc.da1.exception.*;
import com.hsbc.da1.model.*;

public class ArrayBackedSavingsAccountDAOImpl implements SavingsAccountDAO{

	private static List<SavingsAccount> salist = new ArrayList<>();

	@Override
	public SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount) {
		salist.add(savingsAccount);
		return savingsAccount;
	}

	@Override
	public void deleteSavingsAccount(long accountNumber) {
		for(SavingsAccount sa : salist)
		{
			if(sa.getAccountNumber() == accountNumber)
				salist.remove(sa);
		}		
	}

	@Override
	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount) {
		for(SavingsAccount sa : salist)
		{
			if(sa.getAccountNumber() == accountNumber)
				sa =savingsAccount;
		}
		return savingsAccount;
	}

	@Override
	public SavingsAccount fetchSavingsAccountById(long accountNumber)throws  UserNotFoundException{
		for(SavingsAccount sa : salist)
		{
			if(sa.getAccountNumber() == accountNumber)
				return sa;
		}
		throw new  UserNotFoundException("The requested account does not exist..");
	}

	@Override
	public Collection<SavingsAccount> fetchAllAccounts() {
		return salist;
	}
		
}
