package com.hsbc.da1.dao;

import java.util.Collection;

import com.hsbc.da1.exception.UserNotFoundException;
import com.hsbc.da1.model.SavingsAccount;

public interface SavingsAccountDAO {

	public SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount);
	
	public void deleteSavingsAccount(long accountNumber);
	
	public SavingsAccount updateSavingsAccount(long accountNumber , SavingsAccount savingsAccount);
	
	public SavingsAccount fetchSavingsAccountById(long accountNumber)throws  UserNotFoundException;
	
	public Collection<SavingsAccount> fetchAllAccounts();
}
