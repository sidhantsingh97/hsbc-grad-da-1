package com.hsbc.da1.exception;

public class NotEnoughBalanceException extends Exception {
	
	public NotEnoughBalanceException(String message)
	{
		super(message);
	}
	public String getMessage()
	{
		return super.getMessage();
	}

}
