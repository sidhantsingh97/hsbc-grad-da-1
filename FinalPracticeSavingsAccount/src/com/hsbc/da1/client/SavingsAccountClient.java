package com.hsbc.da1.client;

import java.util.Collection;

import com.hsbc.da1.exception.NotEnoughBalanceException;
import com.hsbc.da1.exception.UserNotFoundException;
import com.hsbc.da1.model.SavingsAccount;
import com.hsbc.da1.service.SavingsAccountService;
import com.hsbc.da1.service.SavingsAccountServiceImpl;

public class SavingsAccountClient {
	public static void main(String[] args) {
		
		SavingsAccountService saservice  = new SavingsAccountServiceImpl();
		System.out.println("------------------------------------------------------------------");
		System.out.println("                  Let us create two Savings Account.                ");
		
		SavingsAccount sidhant = saservice.createSavingsAccount("Sidhant Singh", 3000);
		SavingsAccount ranu = saservice.createSavingsAccount("Ranu Sharma", 6000);
		System.out.println("                  Savings Accounts Created                        ");
		System.out.println("------------------------------------------------------------------");
		
		System.out.println("---------------------------DEPOSIT--------------------------------");
		System.out.println("Account Balance of "+sidhant.getAccountName()+" initially is :"+sidhant.getAccountBalance());
		System.out.println("Account Balance of "+ranu.getAccountName()+" initially is :"+ranu.getAccountBalance());
		try{
		saservice.deposit(3483, 7000);
		saservice.deposit(ranu.getAccountNumber(), 4000);
		}catch(UserNotFoundException e)
		{
			System.out.println(e.getMessage());
		}
		System.out.println("Account Balance of "+sidhant.getAccountName()+" after depositing 7000 is :"+sidhant.getAccountBalance());
		System.out.println("Account Balance of "+ranu.getAccountName()+" after depositing 4000 is :"+ranu.getAccountBalance());
		System.out.println("------------------------------------------------------------------\n\n");
		
		System.out.println("---------------------------WITHDRAW--------------------------------");
		System.out.println("Account Balance of "+sidhant.getAccountName()+" initially is :"+sidhant.getAccountBalance());
		System.out.println("Account Balance of "+ranu.getAccountName()+" initially is :"+ranu.getAccountBalance());
		try{
			saservice.withdraw(sidhant.getAccountNumber(), 7000);
			saservice.withdraw(ranu.getAccountNumber(), 4000);
			}catch(UserNotFoundException | NotEnoughBalanceException e)
			{
				System.out.println(e.getMessage());
			}
		System.out.println("Account Balance of "+sidhant.getAccountName()+" after withdrawing 7000 is :"+sidhant.getAccountBalance());
		System.out.println("Account Balance of "+ranu.getAccountName()+" after withdrawing 4000 is :"+ranu.getAccountBalance());
		System.out.println("------------------------------------------------------------------\n\n");
		
		System.out.println("---------------------------TRANSFER--------------------------------");
		System.out.println("Account Balance of "+sidhant.getAccountName()+" initially is :"+sidhant.getAccountBalance());
		System.out.println("Account Balance of "+ranu.getAccountName()+" initially is :"+ranu.getAccountBalance());
		try{
			saservice.transfer(ranu.getAccountNumber(), sidhant.getAccountNumber(), 20000);
			}catch(UserNotFoundException | NotEnoughBalanceException e)
			{
				System.out.println(e.getMessage());
			}
		
		System.out.println("Account Balance of "+sidhant.getAccountName()+" after getting transfered 2000 is :"+sidhant.getAccountBalance());
		System.out.println("Account Balance of "+ranu.getAccountName()+" after transfering 2000 is :"+ranu.getAccountBalance());
		System.out.println("------------------------------------------------------------------\n\n");
		
		System.out.println("------------------FETCHING ALL SAVINGS ACCOUNT---------------------");
		Collection<SavingsAccount> fetchAll = saservice.fetchAllAccounts();
		for(SavingsAccount sa : fetchAll)
		{
			if(sa == null)
			{
				break;
			}
			else
			{
				System.out.println(sa);
			}
		}
		System.out.println("--------------------------------------------------------------------");
		
		
	}	
}
