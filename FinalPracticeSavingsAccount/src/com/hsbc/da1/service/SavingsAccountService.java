package com.hsbc.da1.service;

import java.util.Collection;

import com.hsbc.da1.exception.*;
import com.hsbc.da1.model.*;

public interface SavingsAccountService {

	public SavingsAccount createSavingsAccount(String accountName , double amount);
	
	public void deleteSavingsAccount(long accountNumber)throws  UserNotFoundException;
	
	public SavingsAccount fetchSavingsAccountById(long accountNumber)throws  UserNotFoundException;
	
	public Collection<SavingsAccount> fetchAllAccounts();
	
	public double withdraw(long accountNumber, double amount)throws  UserNotFoundException,NotEnoughBalanceException;
	
	public double deposit(long accountNumber, double amount)throws  UserNotFoundException;
	
	public void transfer(long fromAcc , long toAcc , double amount)throws  UserNotFoundException,NotEnoughBalanceException;
}
