package com.hsbc.da1.service;

import java.util.Collection;

import com.hsbc.da1.dao.ArrayBackedSavingsAccountDAOImpl;
import com.hsbc.da1.dao.SavingsAccountDAO;
import com.hsbc.da1.model.SavingsAccount;
import com.hsbc.da1.exception.*;

public class SavingsAccountServiceImpl implements SavingsAccountService{
	
	SavingsAccountDAO dao = new  ArrayBackedSavingsAccountDAOImpl();

	@Override
	public SavingsAccount createSavingsAccount(String accountName, double amount) {
		
		SavingsAccount sa = new SavingsAccount(accountName, amount);
		
		SavingsAccount sacreated = this.dao.saveSavingsAccount(sa);
		
		return sacreated;
		
	}
	
	@Override
	public void deleteSavingsAccount(long accountNumber) throws  UserNotFoundException{
		SavingsAccount sa = this.dao.fetchSavingsAccountById(accountNumber);
		if(sa!=null)
		{
			this.dao.deleteSavingsAccount(accountNumber);
		}
		else
		{
			System.out.println("NO such user exist");
		}
	}

	@Override
	public SavingsAccount fetchSavingsAccountById(long accountNumber)throws  UserNotFoundException {
		SavingsAccount sa = this.dao.fetchSavingsAccountById(accountNumber);
		return sa;
	}

	@Override
	public Collection<SavingsAccount> fetchAllAccounts() {	
		return this.dao.fetchAllAccounts();
	}

	@Override
	public double withdraw(long accountNumber, double amount)throws  UserNotFoundException,NotEnoughBalanceException {
		SavingsAccount sa = this.dao.fetchSavingsAccountById(accountNumber);
		if(sa!=null)
		{
			double currentAmount = sa.getAccountBalance();
			if(currentAmount>= amount)
			{
				sa.setAccountBalance(currentAmount-amount);
				this.dao.updateSavingsAccount(accountNumber, sa);
				return amount;
			}
			throw new NotEnoughBalanceException("You don't have enough balance");
		}
		return 0;
	}

	@Override
	public double deposit(long accountNumber, double amount)throws  UserNotFoundException {
		SavingsAccount sa = this.dao.fetchSavingsAccountById(accountNumber);
		if(sa!=null)
		{
				double currentAmount = sa.getAccountBalance()+amount;
				sa.setAccountBalance(currentAmount);
				this.dao.updateSavingsAccount(accountNumber, sa);
				return currentAmount;
		}
		return 0;
	}

	@Override
	public void transfer(long fromAcc, long toAcc, double amount)throws  UserNotFoundException,NotEnoughBalanceException {
		//validations
		SavingsAccount saFrom = this.dao.fetchSavingsAccountById(fromAcc);
		SavingsAccount saTo = this.dao.fetchSavingsAccountById(toAcc);
		if(saFrom!=null && saTo!=null)
		{
			double withdrawedAmount = this.withdraw(fromAcc, amount);
			if(withdrawedAmount==amount)
			this.deposit(toAcc, amount);
			else
			{
				System.out.println("Not enough balance to transfer..");
			}
		}
	}
	
}
