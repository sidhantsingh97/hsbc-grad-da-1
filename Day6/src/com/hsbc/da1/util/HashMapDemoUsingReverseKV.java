package com.hsbc.da1.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.hsbc.da1.model.SavingsAccount;

public class HashMapDemoUsingReverseKV {
	
	public static void main(String[] args) {
		Map<SavingsAccount,Integer> mapOfSavingsAccount = new TreeMap<>();
		SavingsAccount deepa = new SavingsAccount("Deepa", 45_000);
		SavingsAccount veena = new SavingsAccount("Veena", 55_000);
		SavingsAccount harsha = new SavingsAccount("Harsha", 65_000);
		SavingsAccount vinay = new SavingsAccount("Vinay", 85_000);
		SavingsAccount kuldeep = new SavingsAccount("Kuldeep", 1_05_000);
		
		mapOfSavingsAccount.put(deepa,1000);
		mapOfSavingsAccount.put(veena,1001);
		mapOfSavingsAccount.put(harsha,1002);
		mapOfSavingsAccount.put(vinay,1003);
		mapOfSavingsAccount.put(kuldeep,1004);
		
		
		//System.out.printf("Size of the map is %d %n", mapOfSavingsAccount.size());
		
		//System.out.println(" Is Kuldeep present "+ ( mapOfSavingsAccount.containsKey(1004)));
		
		Set<SavingsAccount> setOfKeys = mapOfSavingsAccount.keySet();
		
		Iterator<SavingsAccount> keys = setOfKeys.iterator();
		
		while (keys.hasNext()) {
			SavingsAccount key = keys.next();
//			System.out.printf(" Key is %d  %s %n", key, mapOfSavingsAccount.get(key));
		}
		Collection<Integer> integerValues = mapOfSavingsAccount.values();
		
		Iterator<Integer> its = integerValues.iterator();
		
		Set<Map.Entry<SavingsAccount,Integer>> setOfEntries = mapOfSavingsAccount.entrySet();
		
		Iterator<Map.Entry<SavingsAccount,Integer>> iter = setOfEntries.iterator();
		
		while(iter.hasNext()) {
			Map.Entry<SavingsAccount,Integer> entry = iter.next();
			System.out.printf(" Key is %s  and value is %s %n", entry.getKey().getCustomerName(), entry.getValue());
		}
	}
}
