package com.hsbc.da1.client;

import com.hsbc.da1.controller.SavingsAccountController;
import com.hsbc.da1.model.SavingsAccount ;

public class SavingsAccountClient {
	
	public static void main(String[] args) {
		
		SavingsAccountController sacontroller = new SavingsAccountController();
		System.out.println("------------------------------------------------------------------");
		System.out.println("                  Let us open two Savings Account.                ");
		
		SavingsAccount sidhant = sacontroller.openSavingsAccount("Sidhant Singh", 3000);
		SavingsAccount ranu = sacontroller.openSavingsAccount("Ranu Sharma", 6000);
		System.out.println("                  Savings Accounts Created                        ");
		System.out.println("------------------------------------------------------------------");
		
		System.out.println("---------------------------DEPOSIT--------------------------------");
		System.out.println("Account Balance of "+sidhant.getAccountName()+" initially is :"+sidhant.getAccountBalance());
		System.out.println("Account Balance of "+ranu.getAccountName()+" initially is :"+ranu.getAccountBalance());
		sacontroller.deposit(sidhant.getAccountNumber(), 7000);
		sacontroller.deposit(ranu.getAccountNumber(), 4000);
		System.out.println("Account Balance of "+sidhant.getAccountName()+" after depositing 7000 is :"+sidhant.getAccountBalance());
		System.out.println("Account Balance of "+ranu.getAccountName()+" after depositing 4000 is :"+ranu.getAccountBalance());
		System.out.println("------------------------------------------------------------------\n\n");
		
		System.out.println("---------------------------WITHDRAW--------------------------------");
		System.out.println("Account Balance of "+sidhant.getAccountName()+" initially is :"+sidhant.getAccountBalance());
		System.out.println("Account Balance of "+ranu.getAccountName()+" initially is :"+ranu.getAccountBalance());
		sacontroller.withdraw(sidhant.getAccountNumber(), 7000);
		sacontroller.withdraw(ranu.getAccountNumber(), 4000);
		System.out.println("Account Balance of "+sidhant.getAccountName()+" after withdrawing 7000 is :"+sidhant.getAccountBalance());
		System.out.println("Account Balance of "+ranu.getAccountName()+" after withdrawing 4000 is :"+ranu.getAccountBalance());
		System.out.println("------------------------------------------------------------------\n\n");
		
		System.out.println("---------------------------TRANSFER--------------------------------");
		System.out.println("Account Balance of "+sidhant.getAccountName()+" initially is :"+sidhant.getAccountBalance());
		System.out.println("Account Balance of "+ranu.getAccountName()+" initially is :"+ranu.getAccountBalance());
		sacontroller.transfer(ranu.getAccountNumber(), sidhant.getAccountNumber(), 2000);
		System.out.println("Account Balance of "+sidhant.getAccountName()+" after getting transfered 2000 is :"+sidhant.getAccountBalance());
		System.out.println("Account Balance of "+ranu.getAccountName()+" after transfering 2000 is :"+ranu.getAccountBalance());
		System.out.println("------------------------------------------------------------------\n\n");
		
		System.out.println("------------------FETCHING ALL SAVINGS ACCOUNT---------------------");
		SavingsAccount[] fetchAll = sacontroller.fetchSavingsAccounts();
		for(int index=0;index<fetchAll.length;index++)
		{
			if(fetchAll[index] == null)
			{
				break;
			}
			else
			{
				System.out.println("Customer Name is: "+fetchAll[index].getAccountName()+" with account number :"+fetchAll[index].getAccountNumber()+" has account balance : "+fetchAll[index].getAccountBalance()+"\n");
			}
		}
		System.out.println("--------------------------------------------------------------------");	
	}
}
