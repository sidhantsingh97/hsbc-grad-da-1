package com.hsbc.da1.controller;

import com.hsbc.da1.model.SavingsAccount;
import com.hsbc.da1.service.SavingsAccountService;
import com.hsbc.da1.service.SavingsAccountServiceImpl;
import com.hsbc.da1.util.SavingsAccountServiceFactory;

public class SavingsAccountController {

	private SavingsAccountService savingsAccountService = SavingsAccountServiceFactory.getInstance();

	public SavingsAccount openSavingsAccount(String customerName, double accountBalance) {
		SavingsAccount savingsAccount = this.savingsAccountService
											.createSavingAccount(customerName, accountBalance);	
		return savingsAccount;
	}

	public void deleteSavingsAccount(long accountNumber) {
		this.savingsAccountService.deleteSavingsAccount(accountNumber);
	}

	public SavingsAccount[] fetchSavingsAccounts() {
		SavingsAccount[] accounts = this.savingsAccountService.fetchSavingsAccount();
		return accounts;
	}

	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) {
		SavingsAccount savingsAccount = this.savingsAccountService.fetchSavingsAccountById(accountNumber);
		return savingsAccount;
	}

	public double withdraw(long accountId, double amount) {
		return this.savingsAccountService.withdraw(accountId, amount);
	}
	
	/*public double withdrawFromATM(int pin, double amount) {
		SavingsAccount savingsAccount = this.savingsAccountService.fetchAccountByPIN(pin);
		return this.savingsAccountService.withdraw(savingsAccount.getAccountNumber(), amount);
	}*/

	public double deposit(long accountId, double amount) {
		return this.savingsAccountService.deposit(accountId, amount);
	}

	public double checkBalance(long accountId) {
		return this.savingsAccountService.checkBalance(accountId);
	}
	
	public void transfer(long senderId, long toId, double amount) {
		this.savingsAccountService.transfer( senderId,  toId,  amount);
	}
}
