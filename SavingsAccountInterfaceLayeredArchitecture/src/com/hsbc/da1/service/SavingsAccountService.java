package com.hsbc.da1.service;

import com.hsbc.da1.model.*;
public interface SavingsAccountService {

	public SavingsAccount createSavingAccount(String customerName, double amount);
	
	public void deleteSavingsAccount(long accountNumber);
	
	public SavingsAccount fetchSavingsAccountById(long accountNumber);
	
	public SavingsAccount[] fetchSavingsAccount();
	
	public double deposit(long accountNumber, double amount);
	
	public double withdraw(long accountNumber, double amount);
	
	public void transfer(long fromacc, long toacc,double amount);
	
	public double checkBalance(long accountNumber);
}
