package com.hsbc.da1.model;

public class SavingsAccount {

	private String accountName;
	
	private long accountNumber;
	
	private double accountBalance;
	
	private Address address;
	
	private static int counter = 1000;
	
	public SavingsAccount(String accountName , double accountBalance)
	{
		this.accountName = accountName;
		this.accountNumber = ++counter;
		this.accountBalance = accountBalance;
	}
	
	public SavingsAccount(String accountName , double accountBalance , Address address)
	{
		this.accountName = accountName;
		this.accountNumber = ++counter;
		this.accountBalance = accountBalance;
		this.address = address;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public double getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public long getAccountNumber() {
		return accountNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountName == null) ? 0 : accountName.hashCode());
		result = prime * result + (int) (accountNumber ^ (accountNumber >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SavingsAccount other = (SavingsAccount) obj;
		if (accountName == null) {
			if (other.accountName != null)
				return false;
		} else if (!accountName.equals(other.accountName))
			return false;
		if (accountNumber != other.accountNumber)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SavingsAccount [accountName=" + accountName + ", accountNumber=" + accountNumber + ", accountBalance="
				+ accountBalance + ", address=" + address + "]";
	}
	
	
	
	
}
