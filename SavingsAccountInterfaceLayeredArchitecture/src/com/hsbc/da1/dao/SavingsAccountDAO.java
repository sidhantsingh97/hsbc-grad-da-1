package com.hsbc.da1.dao;

import com.hsbc.da1.model.SavingsAccount;

public interface SavingsAccountDAO {
	
	public SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount);
	
	public void deleteSavingsAccount(long accountNumber);
	
	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount);
	
	public SavingsAccount[] fetchSavingsAccount();
	
	public SavingsAccount fetchSavingsAccountById(long accountNumber);
}