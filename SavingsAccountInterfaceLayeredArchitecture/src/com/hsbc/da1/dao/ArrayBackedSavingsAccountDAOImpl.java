package com.hsbc.da1.dao;

import com.hsbc.da1.model.SavingsAccount;

public class ArrayBackedSavingsAccountDAOImpl implements SavingsAccountDAO {
	
	private static SavingsAccount[] savingsAccounts = new SavingsAccount[100];
	private static int counter = 0;
	

	@Override
	public SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount) {
		// TODO Auto-generated method stub
		savingsAccounts[counter++] = savingsAccount;
		return savingsAccount;
	}

	@Override
	public void deleteSavingsAccount(long accountNumber) {
		// TODO Auto-generated method stub
		for (int index = 0; index < savingsAccounts.length; index ++) {
			if ( savingsAccounts[index].getAccountNumber() == accountNumber) {
				savingsAccounts[index] = null;
				break;
			}
		}
	}

	@Override
	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount) {
		for (int index = 0; index < savingsAccounts.length; index ++) {
			if ( savingsAccounts[index].getAccountNumber() == accountNumber) {
				savingsAccounts[index] = savingsAccount;
				break;
			}
		}
		return savingsAccount;
	}

	@Override
	public SavingsAccount[] fetchSavingsAccount() {
		// TODO Auto-generated method stub
		return savingsAccounts;
	}

	@Override
	public SavingsAccount fetchSavingsAccountById(long accountNumber) {
		// TODO Auto-generated method stub
		for (int index = 0; index < savingsAccounts.length; index ++) {
			if ( savingsAccounts[index].getAccountNumber() == accountNumber) {
				return savingsAccounts[index];
			}
		}
		return null;
	}	
}