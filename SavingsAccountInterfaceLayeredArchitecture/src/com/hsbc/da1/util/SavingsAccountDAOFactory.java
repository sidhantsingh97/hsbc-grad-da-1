package com.hsbc.da1.util;
import com.hsbc.da1.dao.*;
public class SavingsAccountDAOFactory {
	public static SavingsAccountDAO getSavingsAccountDAO( int value) {
		if (value == 1 ) {
			return new ArrayBackedSavingsAccountDAOImpl();
		} else {
			return new ArrayListackedSavingsAccountDAOImpl();
		}
	}
}