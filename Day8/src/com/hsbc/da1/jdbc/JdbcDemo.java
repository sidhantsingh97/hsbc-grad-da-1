package com.hsbc.da1.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class JdbcDemo {

	private static final String INSERT_QUERY = "insert into items (insert into items (name,price)"
			+"values ('Iphone',22000),('Ipad',30000),('MacBookPro',150000)";
	
	public static void main(String[] args) {
		try(Connection connection = DriverManager.
				getConnection("jdbc:derby://localhost:1527/sidhantdatabase","sidhant","password");
				Statement stmt = connection.createStatement();
			){
			int updateRows = stmt.executeUpdate(INSERT_QUERY);
			System.out.println("Number of records updated " + updateRows);
		}catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	
}
