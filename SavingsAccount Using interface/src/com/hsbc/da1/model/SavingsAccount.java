package com.hsbc.da1.model;


public class SavingsAccount extends Object{
	
	private String customerName;
	
	private long accountNumber;
	
	private double accountBalance;
	
	private static long counter = 1000;
	
	public SavingsAccount(String customerName, double accountBalance) {
		this.customerName = customerName;
		this.accountBalance = accountBalance;
		this.accountNumber = ++ counter;
	}

	
	public String getCustomerName() {
		return this.customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public double getAccountBalance() {
		return this.accountBalance;
	}

	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}

	public long getAccountNumber() {
		return accountNumber;
	}

	@Override
	public String toString() {
		return "SavingsAccount [customerName=" + customerName + ", accountNumber=" + accountNumber + ", accountBalance="
				+ accountBalance + "]";
	}
}
