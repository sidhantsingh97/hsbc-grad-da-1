package com.hsbc.da1.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hsbc.da1.model.Item;
import com.hsbc.da1.service.ItemService;

public class FetchServlet extends HttpServlet {
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		
		ItemService itemService = ItemService.getItemService();
		List<Item> items = itemService.fetchItems();
		req.setAttribute("items", items);
		RequestDispatcher rd = req.getRequestDispatcher("list.jsp");
		rd.forward(req, res);
	}

}
