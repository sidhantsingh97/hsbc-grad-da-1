package com.hsbc.da1.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hsbc.da1.model.Item;
import com.hsbc.da1.model.User;
import com.hsbc.da1.service.ItemService;

public class ListItemsServlet extends HttpServlet{
	
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) {
		ItemService itemService = ItemService.getItemService();
		List<Item> items = itemService.fetchItems();
		
		for(Item item: items) {
			System.out.println(item);
		}
	}

	
}
