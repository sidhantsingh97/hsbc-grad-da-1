package com.hsbc.da1.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hsbc.da1.model.Item;
import com.hsbc.da1.service.ItemService;

public class ItemServlet extends HttpServlet {
	
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) {
		String itemName = req.getParameter("name");
		String itemPriceStr = req.getParameter("price");
		
		double price = Double.parseDouble(itemPriceStr);
		Item item = new Item(itemName, price);
		ItemService itemService = ItemService.getItemService();
		itemService.saveItem(item);
		try {
			res.sendRedirect("listitem");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//doGet(req,res);
	}

	
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) {
		ItemService itemService = ItemService.getItemService();
		List<Item> items = itemService.fetchItems();
		
		for(Item item: items) {
			System.out.println(item);
		}
	}

}
