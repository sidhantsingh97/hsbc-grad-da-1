package com.hsbc.da1.client;

import com.hsbc.da1.controller.SavingsAccountController;
import com.hsbc.da1.dao.SavingsAccountDAO;
import com.hsbc.da1.exception.CustomerNotFoundException;
import com.hsbc.da1.exception.InsufficientBalanceException;
import com.hsbc.da1.model.SavingsAccount;
import com.hsbc.da1.service.SavingsAccountService;
import com.hsbc.da1.util.SavingsAccountDAOFactory;
import com.hsbc.da1.util.SavingsAccountServiceFactory;

import static java.lang.System.*;

import java.util.Collection;
import java.util.Scanner;
import java.util.Set;

public class SavingsAccountClient {
	
	public static void main(String[] args) throws InsufficientBalanceException, CustomerNotFoundException {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Please enter your option ==> ");
		System.out.println("1 => Array Backed");
		System.out.println("2 => List Backed");
		System.out.println("3 => Set Backed");
		System.out.println("4 => JDBC Backed");
		
		System.out.println("======================================");
		
		int option = scanner.nextInt();
		
		System.out.println(" Entered option is =  "+ option);
		
		scanner.close();
		
		//wiring
		SavingsAccountDAO dao = SavingsAccountDAOFactory.getSavingsAccountDAO(option);
		
		SavingsAccountService service = SavingsAccountServiceFactory.getInstance(dao);
		
		
		
		SavingsAccountController controller = new SavingsAccountController(service);
		
		/*SavingsAccount sidhant = controller.openSavingsAccount("Sid Singh", 65_000, "sid"+ (int)Math.random()* 100000+"@gmail.com" );//t788766
		System.out.println(sidhant);
		SavingsAccount sharmaranu = controller.openSavingsAccount("Ranu Sharma", 100_000, "ranu"+ (int)Math.random()* 100000+"@gmail.com" );//t788766
		System.out.println(sharmaranu);
		SavingsAccount ashish = controller.openSavingsAccount("Ashish Ranjan", 65_000, "ashish"+ (int)Math.random()* 100000+"@gmail.com" );//t788766
		System.out.println(ashish);
		SavingsAccount riya = controller.openSavingsAccount("Riya", 65_000, "riya"+ (int)Math.random()* 100000+"@gmail.com" );//t788766
		System.out.println(riya);
		SavingsAccount sneha = controller.openSavingsAccount("Sneha", 65_000, "sneha"+ (int)Math.random()* 100000+"@gmail.com" );//t788766
		System.out.println(sneha);
		SavingsAccount sayan = controller.openSavingsAccount("Sayan", 65_000, "sayan"+ (int)Math.random()* 100000+"@gmail.com" );//t788766
		System.out.println(sayan);
		//out.println("Account Id "+ kiranSavingsAccount.getAccountNumber());*/
		
		/*System.out.println("\n\n\n-------------------------------FETCHING ACCOUNT BY ACCCOUNT ID-----------------------\n\n\n");
		SavingsAccount ranuFetched =controller.fetchSavingsAccountByAccountId(23);
		System.out.println(ranuFetched);
		System.out.println("\n\n\n----------------------------------------------------------------------------------------\n\n\n");
		*/
		/*System.out.println("\n\n\n------------------------------- DELETING ACCOUNT BY ACCCOUNT ID-----------------------\n\n\n");
		controller.deleteSavingsAccount(23);
		System.out.println("\n\n\n----------------------------------------------------------------------------------------\n\n\n");
		*/
		
		/*System.out.println("\n\n\n------------------------------- UPDATING ACCOUNT BY ACCCOUNT ID-----------------------\n\n\n");
		try {
		SavingsAccount saUpdated = controller.updateAccount("Ranu Sharma", 50000, "ranu@gmail.com", 22);
		System.out.println(saUpdated);
		}catch(CustomerNotFoundException e)
		{
			System.out.println(e.getMessage());
		}
		System.out.println("\n\n\n----------------------------------------------------------------------------------------\n\n\n");
		*/
		
		System.out.println("\n\n\n------------------------------- FETCHING ALL ACCOUNTS-----------------------\n\n\n");
		Collection<SavingsAccount> sa =controller.fetchSavingsAccounts();
		for(SavingsAccount index : sa)
		{
			if(index==null)
			{
				break;
			}
			else
			{
				System.out.println(index);
			}
		}
		System.out.println("\n\n\n----------------------------------------------------------------------------------------\n\n\n");
		
		
	}
}