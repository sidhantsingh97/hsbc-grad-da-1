package com.hsbc.da1.util;

import com.hsbc.da1.dao.JdbcBackedDAOImpl;
import com.hsbc.da1.dao.SavingsAccountDAO;
import com.hsbc.da1.dao.SetBackedSavingsAccountDAOImpl;

public final class SavingsAccountDAOFactory {

    private SavingsAccountDAOFactory() {
    }

    public static SavingsAccountDAO getSavingsAccountDAO(int value) {
        switch (value) {
        	case 1:
        		return new JdbcBackedDAOImpl();
        	case 2:
        		return new SetBackedSavingsAccountDAOImpl();
        	case 3:
        		return new JdbcBackedDAOImpl();
        	default:
        		return new JdbcBackedDAOImpl();
        }
    }
}
