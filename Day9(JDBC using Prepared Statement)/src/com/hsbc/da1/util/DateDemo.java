package com.hsbc.da1.util;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class DateDemo {
	
	public static void main(String[] args) {
		
		Scanner s = new Scanner(System.in);
		
		String dateOfBirth = s.next();
		System.out.println("Enter your date of birth in dd-mm-yy format..");
		LocalDate dob = LocalDate.parse(dateOfBirth, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
		
		LocalDate currentDate = LocalDate.now();
		long noOfDays = Period.between(dob,currentDate).getDays();
		System.out.println("The number of days between my D.O.B and Currentdate is :"+noOfDays);
	}	
	
}
