package com.hsbc.da1.client;

import com.hsbc.da1.controller.SavingsAccountController;
import com.hsbc.da1.model.SavingsAccount;

public class SavingsAccountClient {
	
	public static void main(String[] args) {
		
		SavingsAccountController controller = new SavingsAccountController();
		
		SavingsAccount kiranSavingsAccount = controller.openSavingsAccount("Kiran", 25_000);
		SavingsAccount rajeshSavingsAccount = controller.openSavingsAccount("Rajesh", 45_000);
		
		controller.transfer(kiranSavingsAccount.getAccountNumber(), rajeshSavingsAccount.getAccountNumber(), 45000);
		
		System.out.println("Account Id "+ kiranSavingsAccount.getAccountNumber());
		System.out.println("Account Id "+ rajeshSavingsAccount.getAccountNumber());
		
		double newAccBalance = controller.deposit(1001, 20000);
		System.out.println("Account Balance: " + newAccBalance);
		
		SavingsAccount[] savingsAccounts = controller.fetchSavingsAccounts();
		
		for(SavingsAccount savingsAccount: savingsAccounts) {
			if ( savingsAccount != null) {
				System.out.println("Account Id : "+ savingsAccount.getAccountNumber());
				System.out.println("Account Name : "+ savingsAccount.getCustomerName());
			}
		}
	}
}
