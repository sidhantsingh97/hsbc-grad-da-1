package com.hsbc.da1.service;

import com.hsbc.da1.dao.SavingsAccountDAO;
import com.hsbc.da1.model.*;

public class SavingsAccountService {

	private SavingsAccountDAO dao = new SavingsAccountDAO();
	
	public SavingsAccount createSavingsAccount(String customerName , double accountBalance)
	{
		SavingsAccount sa = new SavingsAccount(customerName, accountBalance);
		SavingsAccount sacreated = dao.saveSavingsAccount(sa);
		return sacreated;
	}
	
	public SavingsAccount createSavingsAccount(String customerName , double accountBalance , Address address)
	{
		SavingsAccount sa = new SavingsAccount(customerName,accountBalance,address);
		
		SavingsAccount sacreated = this.dao.saveSavingsAccount(sa);
		
		return sacreated;
	}
	
	public void deleteSavingsAccount(long accountNumber)
	{
		this.dao.deleteSavingsAccount(accountNumber);
	}
	
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber)
	{
		SavingsAccount sa =  this.dao.fetchSavingsAccountById(accountNumber);
		return sa;
	}
	public SavingsAccount[] fetchSavingsAccount()
	{
		SavingsAccount[] sa  = dao.fetchSavingsAccount();
		return sa;
	}
	
	public double withdraw(long accountNumber , double amount)
	{
		SavingsAccount sa = this.dao.fetchSavingsAccountById(accountNumber);
		if(sa!=null)
		{
			if(sa.getAccountBalance()>amount)
			{
				double currentAccountBalance = sa.getAccountBalance() - amount;
				sa.setAccountBalance(currentAccountBalance);
				this.dao.updateSavingsAccount(accountNumber, sa);
				return amount;
			}			
		}
		return 0;
	}
	
	public double deposit(long accountNumber , double amount)
	{
		SavingsAccount sa = this.dao.fetchSavingsAccountById(accountNumber);
		if(sa!=null)
		{
			double currentBalance = sa.getAccountBalance() + amount;
			sa.setAccountBalance(currentBalance);
			this.dao.updateSavingsAccount(accountNumber, sa);
			return currentBalance;
		}
		return 0;
	}
	
	public void transfer(long fromacc ,long toacc, double amount)
	{
		SavingsAccount sato = this.dao.fetchSavingsAccountById(toacc);
		SavingsAccount safrom = this.dao.fetchSavingsAccountById(fromacc);
		if(sato!=null && safrom!=null)
		{
			double widrawnamount = this.withdraw(safrom.getAccountNumber(), amount);
			if(widrawnamount!=0)
			{
				this.deposit(sato.getAccountNumber(), amount);
				this.dao.updateSavingsAccount(fromacc, safrom);
				this.dao.updateSavingsAccount(toacc, sato);
			}
		}
		else
		{
			System.out.println("Not enough amount to transfer.");
		}
	}
	public double checkBalance(long accountNumber)
	{
		SavingsAccount sa = this.dao.fetchSavingsAccountById(accountNumber);
		if(sa!=null)
		{
			return sa.getAccountBalance();
		}
		return 0;
	}
}
