package com.hsbc.da1.dao;

import com.hsbc.da1.model.SavingsAccount;

public class SavingsAccountDAO {

	SavingsAccount[] sa = new SavingsAccount[100];
	private static int counter = 0;
	
	public SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount)
	{
		this.sa[counter++] = savingsAccount;
		return savingsAccount;
	}
	
	public void deleteSavingsAccount(long accountNumber)
	{
		for(int index = 0 ; index<sa.length ; index++)
		{
			if(sa[index].getAccountNumber() == accountNumber)
			{
				sa[index] = null;
				break;
			}
		}
	}
	
	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount)
	{
		for(int index = 0;index < sa.length;index++)
		{
			if(sa[index].getAccountNumber()==accountNumber)
			{
				sa[index]=savingsAccount;
				break;
			}
		}
		return savingsAccount;
	}
	
	public SavingsAccount[] fetchSavingsAccount()
	{
		return sa;
	}
	
	public SavingsAccount fetchSavingsAccountById(long accountNumber)
	{
		for(int index = 0;index < sa.length;index++)
		{
			if(sa[index].getAccountNumber()==accountNumber)
			{
				return sa[index];
			}
		}
		return null;
	}
}
