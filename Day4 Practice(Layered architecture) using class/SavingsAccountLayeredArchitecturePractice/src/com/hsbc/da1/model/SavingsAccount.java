package com.hsbc.da1.model;

public class SavingsAccount {

	private String accountName;
	
	private long accountNumber;
	
	private double accountBalance;
	
	private Address address;
	
	private static int counter = 1000;
	
	public SavingsAccount(String accountName , double accountBalance)
	{
		this.accountName = accountName;
		this.accountNumber = ++counter;
		this.accountBalance = accountBalance;
	}
	
	public SavingsAccount(String accountName , double accountBalance , Address address)
	{
		this.accountName = accountName;
		this.accountNumber = ++counter;
		this.accountBalance = accountBalance;
		this.address = address;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public double getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public long getAccountNumber() {
		return accountNumber;
	}
	
}
