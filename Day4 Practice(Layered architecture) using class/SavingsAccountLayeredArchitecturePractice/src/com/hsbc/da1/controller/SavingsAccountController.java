package com.hsbc.da1.controller;

import com.hsbc.da1.model.SavingsAccount;
import com.hsbc.da1.service.SavingsAccountService;
import com.hsbc.da1.model.Address;

public class SavingsAccountController {

	private SavingsAccountService saservice = new SavingsAccountService();
	
	public SavingsAccount openSavingsAccount(String customerName , double accountBalance)
	{
		SavingsAccount sa = this.saservice.createSavingsAccount(customerName, accountBalance);
		return sa;
	}
	
	public SavingsAccount openSavingsAccount(String customerName , double accountBalance , Address address)
	{
		SavingsAccount sa = this.saservice.createSavingsAccount(customerName, accountBalance,address);
		return sa;
	}
	
	public void deleteSavingsAccount(long accountNumber)
	{
		this.saservice.deleteSavingsAccount(accountNumber);
	}
	
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber)
	{
		SavingsAccount sa =  this.saservice.fetchSavingsAccountByAccountId(accountNumber);
		return sa;
	}
	public SavingsAccount[] fetchSavingsAccount()
	{
		SavingsAccount[] sa  = saservice.fetchSavingsAccount();
		return sa;
	}
	
	public double withdraw(long accountNumber , double amount)
	{
		double withdrawnamount = this.saservice.withdraw(accountNumber, amount);
		return withdrawnamount;
		
	}
	
	public double deposit(long accountNumber , double amount)
	{
		double currentbalance = this.saservice.deposit(accountNumber, amount);
		return currentbalance;
	}
	
	public void transfer(long fromacc ,long toacc, double amount)
	{
		this.saservice.transfer(fromacc, toacc, amount);
	}
	public double checkBalance(long accountNumber)
	{
		double balance = this.saservice.checkBalance(accountNumber);
		return balance;
	}
}
