package com.hsbc.da1.inventory;

public class DeadlockDemo {
	
	private static final Message lock1 = new Message("Lock1");
	
	private static final Message lock2 = new Message ("Lock2");
	

	public static void main(String[] args) {
		
		Job thread1 = new Job(lock1, lock2);
		thread1.setName("task1");
		
		Job2 thread2 = new Job2(lock1, lock2);
		thread2.setName("task2");
		
		thread1.start();
		thread2.start();
		
		try {
			thread1.join();
			thread2.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
}

class Job extends Thread {
	
	private Message job1;
	
	private Message job2;
	
	public Job ( Message message1, Message message2) {
		this.job1 = message1;
		this.job2 = message2;
	}
	
	@Override 
	public void run() {
		
		acquireLock();
	}

	private void acquireLock() {
		
		synchronized (job1) {
			System.out.println(" Acquired the lock on Job1: "+ Thread.currentThread().getName());
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println(" Now acquiring lock on second job: " + Thread.currentThread().getName());
			synchronized (job2) {
				System.out.println(" Now acquired lock on job 1 and job 2 ...");
			}
			System.out.println( " Releasing the lock 2: "+ Thread.currentThread().getName());
		}
		System.out.println( " Releasing the lock 1: "+ Thread.currentThread().getName());
	}
}

class Job2 extends Thread {
	
	private Message job1;
	
	private Message job2;
	
	public Job2 ( Message message1, Message message2) {
		this.job1 = message1;
		this.job2 = message2;
	}
	
	@Override 
	public void run() {
		
		acquireLock();
	}

	private void acquireLock() {
		
		synchronized (job1) {
			System.out.println(" Acquired the lock on Job1: "+ Thread.currentThread().getName());
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println(" Now acquiring lock on second job: " + Thread.currentThread().getName());
			synchronized (job2) {
				System.out.println(" Now acquired lock on job 1 and job 2 ...");
			}
			System.out.println( " Releasing the lock 2: "+ Thread.currentThread().getName());
		}
		System.out.println( " Releasing the lock 1: "+ Thread.currentThread().getName());
	}
}


class Message {
	private String msg;
	
	public Message(String msg) {
		this.msg = msg;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}
