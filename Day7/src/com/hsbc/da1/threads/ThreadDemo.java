package com.hsbc.da1.threads;

public class ThreadDemo {
	public static void main(String[] args) {
		

		Thread t = Thread.currentThread();
		System.out.println("Name of the Thread "+t.getName());
		for(int i=0;i<10;i++)
		{
			System.out.println("The name of the Thread is :"+t.getName());
			try {
				t.sleep(20000);
			}catch(InterruptedException e)
			{
				System.out.println("Thread interrupted while in sleep...");
			}
		}
	}
}
