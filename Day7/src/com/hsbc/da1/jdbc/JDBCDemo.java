package com.hsbc.da1.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBCDemo {

	public static void main(String[] args) {
		
		try(Connection connection = DriverManager.getConnection("jdbc:derby://localhost:1527/sidhantdatabase","sidhant","password");
				Statement stmt = connection.createStatement();
			){
			
			//int count = stmt.executeUpdate("Insert into items (name , price) values ('MacBookPro',100000)");
			//count = stmt.executeUpdate("Insert into items (name , price) values ('iWatch',30000)");
			//System.out.println("Number of items inserted :: "+ count);
			
			ResultSet resultSset = stmt.executeQuery("select * from items");
			
			while(resultSset.next())
			{
				System.out.println(resultSset.getString(1));
				System.out.println(resultSset.getDouble(2));
			}
		}catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	
}
