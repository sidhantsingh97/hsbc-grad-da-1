package com.hsbc.da1.assignment2.threads;



public class RunnableImplementation {

public static void main(String[] args) {
		
		Runnable gphotos = new GooglePhotos();
		Runnable pic = new Picassa();
		Runnable flic = new Flickr();
		
		Thread googlePic = new Thread (gphotos);
		googlePic.setName("Google-Photos");
		
		Thread picassaPic = new Thread (pic);
		picassaPic.setName("Picassa");
		
		Thread flickrPic = new Thread (flic);
		flickrPic.setName("Flickr");
		
		googlePic.start();
		picassaPic.start();
		flickrPic.start();
		
		try {
			googlePic.join();
			picassaPic.join();
			flickrPic.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("All Pics Are Downloaded");
		
	}

}

class GooglePhotos implements Runnable {

	@Override
	public void run() {
		System.out.println("============ Thread "+ Thread.currentThread()+ " start ===========");
		
		for(int i = 0 ;i < 10; i ++) {
			System.out.println("Thread running "  + i + " name: " + Thread.currentThread());
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("============ Thread "+ Thread.currentThread()+ " end ===========");
	}
}

class Picassa implements Runnable {

	@Override
	public void run() {
		System.out.println("============ Thread "+ Thread.currentThread()+ " start ===========");
		
		for(int i = 0 ;i < 10; i ++) {
			System.out.println("Thread running "  + i + " name: " + Thread.currentThread());
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("============ Thread "+ Thread.currentThread()+ " end ===========");
	}
}

class Flickr implements Runnable {

	@Override
	public void run() {
		System.out.println("============ Thread "+ Thread.currentThread()+ " start ===========");
		
		for(int i = 0 ;i < 10; i ++) {
			System.out.println("Thread running "  + i + " name: " + Thread.currentThread());
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("============ Thread "+ Thread.currentThread()+ " end ===========");
	}
}
