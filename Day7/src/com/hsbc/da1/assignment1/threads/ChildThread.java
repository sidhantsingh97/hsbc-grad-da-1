package com.hsbc.da1.assignment1.threads;

public class ChildThread extends Thread{
	
	public static void main(String[] args) {
		System.out.println(" Main Thread when the program is starting -> "+ Thread.currentThread().getName());
		
		//object initialization 
		ChildThread t  = new ChildThread();
		t.setName("thread - 1 ");
		//Spawning a new thread
		t.start();
		
		for ( int i = 0; i < 5; i ++) {
			System.out.println("Inside the main thread "+ Thread.currentThread().getName());
			try {
				Thread.currentThread().sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println(" Main Thread is completed");
		try {
			t.join();
		}catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@Override
	public void run () {
		System.out.println("==================Thread " + Thread.currentThread().getName() + " start ==================");
		for ( int i = 0; i < 10; i ++) {

			System.out.println(" Name of the Thread => "+ Thread.currentThread().getName());
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("==================Thread " + Thread.currentThread().getName() + " terminates ==================");
	}
}
