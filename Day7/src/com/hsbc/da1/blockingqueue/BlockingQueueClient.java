package com.hsbc.da1.blockingqueue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class BlockingQueueClient {
	
	public static void main(String[] args) {
		BlockingQueue<Message> queue = new ArrayBlockingQueue<>(10);
		
		Producer producer = new Producer(queue);
		Consumer consumer = new Consumer(queue);
		
		ExecutorService es = Executors.newFixedThreadPool(2);
		
		new Thread(producer).start();
		new Thread(consumer).start();
		
		System.out.println(":: Both producer and consumer threads are now started ::");
	}

}
