package com.hsbc.da1.concurrency;

public class PrinterDemoSamePrinter {

	public static void main(String[] args) {
		Printer printer1 = new Printer();
		//Printer printer2 = new Printer();
		//Printer printer3 = new Printer();

		Job job1 = new Job(printer1, 10, 15);
		Job job2 = new Job(printer1, 50, 60);
		Job job3 = new Job(printer1, 80, 95);

		Thread task1 = new Thread(job1);
		Thread task2 = new Thread(job2);
		Thread task3 = new Thread(job3);

		task1.start();
		task2.start();
		task3.start();

		try {
			task1.join();
			task2.join();
			task3.join();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(" Completed all the tasks");
	}
}

class Printer {

	public synchronized void print(int start, int end) throws InterruptedException {
		if (end >= start) {
			System.out.println(" Printing  the " + start + " page");
			for (int i = start; i < end; i++) {
				System.out.println(" Printing  the " + i + " page");
				Thread.sleep(2000);
			}
			System.out.println(" Printing the "+ end + " page");
		}
	}
}

class Job implements Runnable {

	private Printer printer;
	int start;
	int end;

	public Job(Printer printer, int start, int end) {
		this.printer = printer;
		this.start = start;
		this.end = end;
	}

	/*@Override
	public void run() {
		try {
			synchronized (printer) {
				printer.print(start, end);	
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}*/
	
	@Override
	public void run() {
		try {
				printer.print(start, end);	
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
