package com.hsbc.da1.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FileReaderDemo {
	
	public static void main(String[] args) {
		
		FileReader filereader =null;
		
		try
		{
			File textFile = new File("D:\\xyz.txt");
			filereader = new FileReader(textFile);
			boolean endOfFile =false;
			
			while(!endOfFile)
			{
				int character = filereader.read();
				if(character!=-1)
				{
					char c = (char)character;
					System.out.println(c);
				}
				else
				{
					endOfFile = true;
				}
			}
		}catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}finally {
			if(filereader!=null) {
				try {
					filereader.close();
				}catch(IOException e)
				{
					e.printStackTrace();
				}
				
			}
		}
	}

}
